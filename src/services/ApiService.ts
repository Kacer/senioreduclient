export class ApiService {
    public static readonly GET: string = "get";
    public static readonly POST: string = "post";
    public static readonly PUT: string = "put";
    public static readonly DELETE: string = "delete";

    public static readonly SERVER_PREFIX: string = "http://localhost:55171"

    static header(): Headers{
        return new Headers({
            "Content-Type": "application/json",
            Accept: "application/json",
            'Origin': 'http://localhost:3000'
        })
    }
    
    static async call(postfix:string, type:string): Promise<Response>{
        return await fetch(this.SERVER_PREFIX + "/api/" + postfix, {
            method: type,
            headers: this.header(),
            mode: 'cors',
            credentials: 'include'
        })
    }

    static async callWithBody(postfix:string, type:string, jsonData:any): Promise<Response>{
        return await fetch(this.SERVER_PREFIX + "/api/" + postfix, {
            method: type,
            headers: this.header(),
            body: JSON.stringify(jsonData),
            mode: 'cors',
            credentials: 'include'
        })
    }

    static async callWithFile(postfix:string, type:string, file:File): Promise<Response>{
        let formData = new FormData();
        formData.append("file", file);
        
        return await fetch(this.SERVER_PREFIX + "/api/" + postfix, {
            method: type,
            body: formData,
            mode: 'cors',
            credentials: 'include'
        })
    }
}

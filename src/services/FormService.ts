export class FormService {
    
    static getStateFromEvent(event:any): any{
        const target = event.target;
        const value = target.value;
        const name = target.name;
        const newState = { [name]: value };
        return newState;
    }
    
}

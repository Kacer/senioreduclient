import { ApiService } from "./ApiService";

export class UserService {
    static logOut(){
        let response: any = ApiService.call("LoginApi/logout", ApiService.POST);
        return response.status === 200;
    }
}

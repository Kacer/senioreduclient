import React from 'react';
import './App.css';
import './css/Site.css'
import 'jquery/dist/jquery'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle';
import MainContainerComponent from './components/Home/MainContainerComponent';
import { Route, BrowserRouter, Switch, Redirect } from 'react-router-dom';
import RegisterComponent from './components/Home/Register/RegisterComponent';
import HelloComponent from './components/HelloComponent';
import LoginComponent from './components/Home/Login/LoginComponent';
import AdminLayoutComponent from './components/Admin/AdminLayoutComponent';
import UserLayoutComponent from './components/User/UserLayoutComponent';


function test(p: any) {
  let test:any = localStorage.getItem('user');
  if (localStorage.getItem('user')) { return <HelloComponent ownProp={"test"} /> }
  else {
    return <Redirect to={{ pathname: '/Login', state: { from: p.location } }} />;
  }
}

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/Admin" component={AdminLayoutComponent}/>
          <Route path="/User" component={UserLayoutComponent}/>
          <Route path="/Login/test" render={
            (p) =>
              test(p)
          }>
          </Route>
          <Route path="/Login">
            <LoginComponent />
          </Route>
          <Route path="/Register">
            <RegisterComponent />
          </Route>
          <Route path="/">
            <MainContainerComponent date={new Date().getFullYear().toString()} defCom={"h"} />
          </Route>
        </Switch>
      </BrowserRouter>
    </div >
  );
}

export default App;

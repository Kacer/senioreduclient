import React from "react";

interface Prop {
    handler: any
}

interface State {
    // empty
}

class UserHomeSubProfilComponent extends React.Component<Prop, State> {
    render() {
        return (
            <div className="container-fluid dashboard-content">
                <h2>Jak pracovat se záložkou Profil</h2>
                <p>Pokud rozkliknete záložku <strong>Profil</strong> ukáže se Vám zhruba takovýto pohled:</p>
                <br />
                <img src="/images/uvodniStranka/profil/profile.jpg" className="img-fluid" alt="Profil" />
                <br />
                <p>Vidíte zde několik záložek v levém bočním menu <i>Profil, Upravit profil a Moje Testy</i>, tyto záložky si nyní postupně přiblížíme.</p>
                <p>
                    Jak je vidno, v první záložce <strong>Profil</strong> jsou Vaše údaje, jako jsou jméno, příjmení, datum narození, registrace a datum posledního
            přihlášení na profilu. Tyto informace vidíte jen Vy a naši administrátoři, takže se nemusíte bát.
        </p>
                <p>
                    Pokud chcete Váš profil upravit, přesunete se na další záložku, na obrázku je pro lepší přehlednost tato záložka podtržena
                    zelenou barvou.
        </p>
                <br />
                <img src="/images/uvodniStranka/profil/uprava-profilu.jpg" className="img-fluid" alt="Uprava profilu" />
                <br />
                <p>
                    V modrém rámečku je Vaše jméno a příjmení, ta musí být vyplněná pokaždé. Proto je měňte pouze, když si všimnete, že jste
                    udělali ve svém jméně překlep. V oranžovém rámečku můžete, ale nemusíte vyplnit Vaše datum narození. Toto datum opět uvidíte jenom
                    Vy a nebo administrátor těchto stránek. V růžovém rámečku si můžete vybrat profilovou fotku, pakliže nějaké fotky máte ve svém
                    počítači. Pokud nevíte, jak se taková fotka nahraje. Nezoufejte, budete si to moct nastudovat v některém z našich učebních článků.
        </p>
                <p>
                    Rámečky jsou zde samozřejmě jen pro lepší přehlednost. Když upravíte své osobní údaje, není nic lehčího, než kliknout na tlačítko 
            <strong> Upravit profil</strong> a Vaše nové informace se uloží na Váš profil. Toto talčítko je zde v červeném rámečku.
        </p>
                <br />
                <img src="/images/uvodniStranka/profil/profil-testy.jpg" className="img-fluid" alt="Uprava profilu" />
                <br />
                <p>
                    Až se naučíte pracovat s články, které si nastudujete. Můžete si ověřit své znalosti pomocí jednoduchých krátkých testů.
            Výsledky těchot testů, datum jejich vyplnění, bodové a procentuální hodnocení uvidíte v záložce <strong>Moje testy</strong>.
        </p>
                <div className="form-group">
                    <div className="col-sm-2 col-sm-10">
                        <a className="btn btn-primary" href="javascript:void(0);" onClick={()=>this.props.handler("profil",true)}>Podívat se na Profil</a>
                        <a className="btn btn-primary" href="javascript:void(0);" onClick={()=>this.props.handler("home",false)}>Zpět na hlavní stránku</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default UserHomeSubProfilComponent;
import React from "react";

interface Prop {
    handler: any
}

interface State {
    // empty
}

class UserHomeSubSlovnikComponent extends React.Component<Prop, State> {
    render() {
        return (
            <div className="container-fluid dashboard-content">
                <h2>Jak pracovat se záložkou Slovníček</h2>
                <p>Pokud rozkliknete záložku <strong>Slovníček</strong> ukáže se Vám zhruba takovýto pohled:</p>
                <br />
                <img src="/images/uvodniStranka/slovnicek/slovnicek.jpg" className="img-fluid" alt="Slovnicek" />
                <br />
                <p>Zeleně podtržené je slovíčko a vedle něj je jeho stručný význam.</p>
                <div className="form-group">
                    <div className="col-sm-2 col-sm-10">
                        <a className="btn btn-primary" href="javascript:void(0);" onClick={()=>this.props.handler("slovnik",true)}>Podívat se do Slovníčku</a>
                        <a className="btn btn-primary" href="javascript:void(0);" onClick={()=>this.props.handler("home",false)}>Zpět na hlavní stránku</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default UserHomeSubSlovnikComponent;
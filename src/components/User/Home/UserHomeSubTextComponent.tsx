import React from "react";

interface Prop {
    handler: any
}

interface State {
    // empty
}

class UserHomeSubTextComponent extends React.Component<Prop, State> {
    render() {
        return (
            <div className="container-fluid dashboard-content">
                <h2>Jak pracovat se záložkou Učební texty</h2>
                <p>Pokud rozkliknete složku <strong>Učební texty</strong> ukáže se Vám zhruba takovýto pohled:</p>
                <br />
                <img src="/images/uvodniStranka/vyukoveTexty/text-main.jpg" className="img-fluid" alt="Clanky" />
                <br />
                <p>
                    Na levé straně v zeleném rámečku vidíte několik záložek. To jsou názvy kategorií tetxů, ve kterých jsou zeřazeny
                    jednotlivé články právě podle těchto kategorií. Záložky se mohou měnit, protože pro Vás chceme neustále zlepšovat
                    články, ze kterých se můžete učit.
        </p>
                <p>
                    Uprostřed je tabulka, kde vidíte fialově podtržený text. To je název článku, na který když kliknete, zobrazí se Vám
                    jeho obsah a vy si můžete číst a vzdělávat se. Obsah článku vypadá nějak takto:
        </p>
                <img src="/images/uvodniStranka/vyukoveTexty/text-detail.jpg" className="img-fluid" alt="Detail článku" />
                <br />
                <p>
                    Na prvním obrázku ještě vidíte v každém řádku jednotlivých článků políčko <strong>Test</strong>, na který, když kliknete, 
                    zahájíte test pro vybraný článek a zobrazí se Vám zhruba takový pohled:
        </p>
                <img src="/images/uvodniStranka/vyukoveTexty/test-start.jpg" className="img-fluid" alt="Test" />
                <br />
                <p>
                    Vždy si musíte přečíst otázku a k ní vybrat odpvěď. Odpověď může být buď jedna a nebo více, pokud stále nechcete psát test, vraťte se stisknutím
            tlačítka vlevo <strong>Zpět na všechny články</strong>. Pokud jste test vyplnil/a, stiskněte dole pod testem modré tlačítko <strong>Ukončit test</strong>.
            Po stisknutí tohoto tlačítka se Vám zobrazí stránka, kde bude test vyhodnocen:
        </p>
                <img src="/images/uvodniStranka/vyukoveTexty/vyhonocenyTest.jpg" className="img-fluid" alt="Vyhodnocení testu" />
                <br />
                <p>
                    Zde vidíte zeleně označené správné odpovědi a červeně označené špatné odpovědi. Pokud odpověď svítí červeně, ale není označená,
                    znamená to, že to byla správná odpověď.
        </p>
                <div className="form-group">
                    <div className="col-sm-2 col-sm-10">
                        <a className="btn btn-primary" href="javascript:void(0);" onClick={()=>this.props.handler("text",true)}>Podívat se do Učebních textů</a>
                        <a className="btn btn-primary" href="javascript:void(0);" onClick={()=>this.props.handler("home",false)}>Zpět na hlavní stránku</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default UserHomeSubTextComponent;
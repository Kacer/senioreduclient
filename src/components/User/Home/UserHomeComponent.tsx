import React from "react";
import UserHomeSubDefaultComponent from "./UserHomeSubDefaultComponent";
import UserHomeSubProfilComponent from "./UserHomeSubProfilComponent";
import UserHomeSubTextComponent from "./UserHomeSubTextComponent";
import UserHomeSubSlovnikComponent from "./UserHomeSubSlovnikComponent";
import UserHomeSubNapisComponent from "./UserHomeSubNapisComponent";

interface Prop {
    parentHandler: any;
}

interface State {
    content: string;
}

class UserHomeComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { content: "home" };
        this.handleChildInfo = this.handleChildInfo.bind(this);
    }

    renderBody(page: string) {
        switch (page) {
            case "home":
                return (
                    <UserHomeSubDefaultComponent />
                )
            case "profil":
                return (
                    <UserHomeSubProfilComponent handler={this.handleChildInfo} />
                )
            case "text":
                return (
                    <UserHomeSubTextComponent handler={this.handleChildInfo} />
                )
            case "slovnik":
                return (
                    <UserHomeSubSlovnikComponent handler={this.handleChildInfo} />
                )
            case "napis":
                return (
                    <UserHomeSubNapisComponent handler={this.handleChildInfo} />
                )
            default:
                return (
                    <></>
                )
        }
    }

    handleChildInfo(info: string, top: boolean) {
        if (top) {
            this.props.parentHandler(info)
        } else {
            this.setState({ content: info });
        }
    }

    render() {
        return (
            <>
                <div>
                    <div className="nav-left-sidebar sidebar-dark">
                        <nav className="navbar navbar-expand-md navbar-light w-100">
                            <ul className="navbar-nav flex-column">
                                <li className="nav-divider">
                                    <h5>Nápověda</h5>
                                </li>
                                <li className="nav-item">
                                    <p>Zde si můžete přečíst nápovědu o tom, jak fungují jednotlivé moduly.</p>
                                </li><li className="nav-item">
                                    <ul className="nav flex-column">
                                        <a className="submenuCat nav-link" href="/User"
                                            onClick={(e) => {e.preventDefault(); this.setState({ content: "profil" }) }}>Jak na Profil?</a>
                                    </ul>
                                </li>
                                <li className="nav-item">
                                    <ul className="nav flex-column">
                                        <a className=" submenuCat nav-link" href="/User"
                                            onClick={(e) => { e.preventDefault(); this.setState({ content: "text" }) }}>Jak na Učební texty?</a>
                                    </ul>
                                </li>
                                <li className="nav-item">
                                    <ul className="nav flex-column">
                                        <a className=" submenuCat nav-link" href="/User"
                                            onClick={(e) => { e.preventDefault(); this.setState({ content: "slovnik" }) }}>Jak na Slovníček?</a>
                                    </ul>
                                </li>
                                <li className="nav-item">
                                    <ul className="nav flex-column">
                                        <a className=" submenuCat nav-link" href="/User"
                                            onClick={(e) => { e.preventDefault(); this.setState({ content: "napis" }) }}>Jak na Napište nám?</a>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <div className="dashboard-wrapper">
                    {this.renderBody(this.state.content)}
                </div>
            </>
        );
    }
}

export default UserHomeComponent;



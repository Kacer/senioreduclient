import React from "react";

interface Prop {
    handler: any
}

interface State {
    // empty
}

class UserHomeSubNapisComponent extends React.Component<Prop, State> {
    render() {
        return (
            <div className="container-fluid dashboard-content">
        <h2>Jak pracovat se záložkou Napište nám</h2>
        <p>Pokud rozkliknete záložku <strong>Napište nám</strong> ukáže se Vám zhruba takovýto pohled:</p>
        <br/>
        <img src="/images/uvodniStranka/napisteNam/email.jpg" className="img-fluid" alt="Forum"/>
        <br/>
        <p>
            Pokud si stále nejste jisti, jak si vytvořit e-mail, abyste mohli poslat zprávu jednomu z našich administrátorů, použíjte
            tlačítko vlevo <strong>Informace o e-mailu</strong> (zde v růžovém rámečku) a najděte si informace o tom, jak se vytváří e-mail.
        </p>
        <p>
            Pokud již máte vytvořený e-mail, vyplňte ho do pole v zeleném rámečku. Pokračujte tak i s vyplněním Předmětu zprávy - toto
            pole není povinné (modrý rámeček) a následně napiště, co chcete našim administrátorům sdělit (pole ve žlutém rámečku).
        </p>
        <p>
            Pokud chcete odeslat zprávu našim administrátorům, stačí po jejím napsání kliknout na talčítko <strong>Odeslat</strong> a zpráva
            se pošle. Pokud čekáte odpověď na zprávu, kontrolujte svou e-mailovou schránku, kam Vám naši administrátoři odpoví.
        </p>
        <div className="form-group">
            <div className="col-sm-2 col-sm-10">
                <a className="btn btn-primary" href="javascript:void(0);" onClick={()=>this.props.handler("napis",true)}>Podívat se do Napište nám</a>
                <a className="btn btn-primary" href="javascript:void(0);" onClick={()=>this.props.handler("home",false)}>Zpět na hlavní stránku</a>
            </div>
        </div>
    </div>
        );
    }
}

export default UserHomeSubNapisComponent;
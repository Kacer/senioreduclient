import React from "react";

interface Prop {
    
}

interface State {
    // empty
}

class UserHomeSubDefaultComponent extends React.Component<Prop, State> {
    render() {
        return (
            <div className="container-fluid dashboard-content">
        <h2>Vítejte, nyní jste přihlášen/a</h2>
        <p>
            Pokud chcete naplno používat tuto stránku, použíjte horní lištu a vyberte si některou ze záložek, kde se můžete začít vzdělávat.
            Záložkami rozumíme - <strong>Profil, Učební texty, Slovníček, Napište nám</strong>, které jsou na obrázku podtrženy
            červenou čárou.
        </p>
        <img src="/images/uvodniStranka/home-lista.jpg" className="img-fluid" alt="Lista"/>
        <br/>
        <br/>
        <p> Pokud si chcete přečíst návod o tom, jak pracovat s jednotlivými záložkami, rozklikněte něktezé z polí v menu nalevo.</p>
        <br/>
        <p>
            Pokud skončíte s prací na této stránce, nezapomeňte se odhlásit tlačítkem <strong>Odhlásit</strong>, které je
            na liště úplně v pravo. Na obrázku je pro lepší přehlednost v červeném rámačku.
        </p>
        <img src="/images/uvodniStranka/home-odhlaseni.jpg" className="img-fluid" alt="Logout"/>
        <br/>
    </div>
        );
    }
}

export default UserHomeSubDefaultComponent;
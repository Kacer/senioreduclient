import React from "react";
import HelloComponent from "../../HelloComponent";
import { ApiService } from "../../../services/ApiService";
import UserTextTableComponent from "./UserTextTableComponent";
import UserArticleDetailComponent from "./UserArticleDetailComponent";

interface Prop {
    parentHandler: any;
}

interface State {
    content: string;
    loading: boolean;
    listCat: Array<any>;
}

class UserTextComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = {
            content: "home",
            loading: true,
            listCat: []
        };
        this.handleChildInfo = this.handleChildInfo.bind(this);
    }


    componentDidMount() {
        this.loadCategories();
    }

    componentWillReceiveProps(){
        this.setState({content: "home"});
    }

    async loadCategories() {
        let path = "category/getAll";
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let cats = await response.json();
            this.setState({ listCat: cats, loading: false });
        } if (response.status === 401) {
            this.setState({ listCat: [], loading: false });
        }
    }

    renderBody(page: string) {
        let id = -1;
        if(page.includes(":")){
            id = parseInt(page.substr(page.indexOf(":")+1));
            page =  page.substr(0, page.indexOf(':')); 
        }
        switch (page) {
            case "home":
                return (
                    <UserTextTableComponent handler={this.handleChildInfo} catId={id} />
                )
            case "articleDetail":
                return (
                    <UserArticleDetailComponent handler={this.handleChildInfo} artId={id} />
                )
            default:
                return (
                    <></>
                )
        }
    }

    handleChildInfo(info: string, top: boolean) {
        if (top) {
            this.props.parentHandler(info)
        } else {
            this.setState({ content: info });
        }
    }

    render() {
        const items: any = [];
        if (!this.state.loading) {
            this.state.listCat.forEach((cat: any) => {
                items.push(
                    <li className="nav-item"  key={cat.Id}>
                        <a className="submenuCat nav-link" href="/User"
                            onClick={(e) => {
                                e.preventDefault();
                                this.setState({content: "home:" + cat.Id});
                            }}
                            title={cat.Description}>
                            {cat.Name}
                        </a>
                        <ul className="nav flex-column pl-3"></ul>
                    </li>
                )
            });
        }

        return (
            <>
                <div className="nav-left-sidebar sidebar-dark">
                    <nav className="navbar navbar-expand-md navbar-light w-100">
                        <ul className="navbar-nav flex-column">
                            <li className="nav-divider">
                                <h5>Výukové texty</h5>
                            </li>
                            <li className="nav-item">
                                <ul className="nav flex-column">
                                    {items}
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div className="dashboard-wrapper">
                    {this.renderBody(this.state.content)}
                </div>
            </>
        );
    }
}

export default UserTextComponent;



import React from "react";
import { ApiService } from "../../../services/ApiService";

interface Prop {

}

interface State {
    loading: boolean;
    tests: any;
}

class UserProfileTestListComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            loading: true,
            tests: {}
        }
    }

    componentDidMount() {
        this.fetchTestsOfLoggedUser();
    }

    fetchTestsOfLoggedUser() {
        ApiService.call('test/getAll', ApiService.GET)
            .then(response => {
                if(response.ok) {
                    response.json()
                        .then(tests => {
                            this.setState({tests: tests, loading: false});
                        }
                    );
                } else {
                    this.setState({loading: false});
                }
            }
        );
    }

    render() {
        if(this.state.loading) {
            return <></>;
        }

        let items: any = [];
        this.state.tests.forEach((test: any) => {
            let articleName = test.ArticleName;
            let formatOptions = { day: 'numeric', month: 'numeric', year: 'numeric', hour: 'numeric', minute: '2-digit', second: '2-digit' };
            let completingTest = new Date(test.CompletingTest).toLocaleDateString('cs-CZ', formatOptions);
            let score = test.Score + '/' + test.MaxScore;
            let successRate = test.Score / test.MaxScore * 100;
            let successRateStyle = {
                color: successRate >= 50 ? 'green' : 'red'
            };

            items.push(
                <tr key={test.Id}>
                    <td>{articleName}</td>
                    <td>{completingTest}</td>
                    <td>{score}</td>
                    <td style={successRateStyle}>{Math.round((successRate + Number.EPSILON) * 100) / 100}%</td>
                </tr>
            );
        });

        return (
            <div className="container-fluid dashboard-content">
                <h2>Vaše Testy</h2>
                    <table className="table">
                        <thead className="thead-dark">
                            <tr>
                                <th scope="col">Název článku</th>
                                <th scope="col">Datum</th>
                                <th scope="col">Body</th>
                                <th scope="col">Úspěšnost</th>
                            </tr>
                        </thead>

                        <tbody>
                            {items}
                        </tbody>
                    </table>
            </div>
        );
    }
}

export default UserProfileTestListComponent;
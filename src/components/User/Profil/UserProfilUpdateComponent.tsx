import React from "react";
import { FormService } from "../../../services/FormService";
import { ApiService } from "../../../services/ApiService";
import { isNull } from "util";

interface Prop {
    handler: any;
    id: number
}

interface State {
    imgFile: any,
    user: any,
    name: string,
    surname: string,
    birth: string,
    image: string,
    loading: boolean,
    imageLoading: boolean,
    imageSrc: string,
    errors: {
        name: string,
        surname: string
    }
}

class UserProfilUpdateComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            imgFile: null,
            user: null,
            name: "",
            surname: "",
            birth: "",
            image: "",
            loading: true,
            imageLoading: true,
            imageSrc: ApiService.SERVER_PREFIX + "/uploads/profilePictures/",
            errors: {
                name: "",
                surname: ""
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.loadUser(this.props.id);
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({ loading: true, user: null });
        this.loadUser(nextProps.id);
    }

    async loadUser(id: number) {
        let path = "user/getById/" + id;
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let u = await response.json();
            let birthSub = '';
            if(u.Birth != null)
                birthSub = u.Birth.substring(0, u.Birth.indexOf("T"));
            this.setState({ user: u, loading: false, name: u.Name, surname: u.Surname, birth: birthSub, image: u.Image });
        } if (response.status === 400 || response.status === 404) {
            this.setState({ user: null, loading: false });
        }
    }

    handleSubmit(event: any) {
        if (this.validateForm(this.state.errors, this.state)) {
            if (!isNull(this.state.imgFile)) {
                this.uploadImage(this.state.imgFile).then(
                    e => {
                        this.sendData(this.state.user);
                    }
                );
            } else {
                this.sendData(this.state.user);
            }
        }
        event.preventDefault();
    }

    handleChange(event: any) {
        const newState = FormService.getStateFromEvent(event);
        this.setState(newState);
        this.validateState(event.target.name, event.target.value);
    }

    validateForm(errors: any, states: any) {
        let valid = true;
        Object.keys(errors).forEach(
            (key: string) => {
                this.validateState(key, states[key]);
            }
        );
        Object.values(errors).forEach(
            (val: any) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    validateState(name: any, value: any) {
        let errorsArray = this.state.errors;
        switch (name) {
            case "name":
                errorsArray.name = value.length < 1 ? 'Jméno je vyžadováno.' : '';
                break;
            case "surname":
                errorsArray.surname = value.length < 1 ? 'Příjmení je vyžadováno.' : '';
                break;
            default:
                break;
        }
        this.setState({ errors: errorsArray });
    }
    async uploadImage(image: File): Promise<boolean> {
        const response = await ApiService.callWithFile("image/upload", ApiService.POST, image);
        if (response.status === 400) {
            console.log("Something is wrong")
        } else if (response.status === 200) {
            let imgName = await response.json();
            this.setState({ image: imgName });
        }
        return response.ok;
    }

    async sendData(user: any): Promise<boolean> {
        user.Name = this.state.name;
        user.Surname = this.state.surname;
        user.Birth = this.state.birth;
        user.Image = this.state.image;
        const response = await ApiService.callWithBody("user/update", ApiService.PUT, user);
        if (response.status === 400) {
            console.log("Something is wrong")
        } else if (response.status === 200) {
            this.props.handler("home:Údaje uživatele byly upraveny.");
        }
        return response.ok;
    }

    loadFile(event: any) {
        this.setState({ imageSrc: URL.createObjectURL(event.target.files[0]), image: "", imgFile: event.target.files[0] })
    };

    render() {
        if (!this.state.loading && !isNull(this.state.user)) {
            return (
                <div className="container-fluid dashboard-content">
                    <h2>Úprava profilu {this.state.user.Login} </h2>

                    <form onSubmit={this.handleSubmit} encType="multipart/form-data" >
                        <div className="container-fluid">
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Jméno</label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="name" type="text" value={this.state.name} onChange={this.handleChange} />
                                    <span className="field-validation-valid"></span>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Příjmení</label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="surname" type="text" value={this.state.surname} onChange={this.handleChange} />
                                    <span className="field-validation-valid"></span>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Datum narození</label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="birth" type="date" value={this.state.birth} onChange={this.handleChange} />
                                    <span className="field-validation-valid" ></span>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Obrázek</label>
                                <div className="col-sm-10">
                                    <input type="file" name="image" accept="image/*" onChange={(e) => this.loadFile(e)} multiple />
                                    <img className="profilEditImg" src={this.state.imageSrc + this.state.image} id="output" alt="Profilový Obrázek" />
                                </div>
                            </div>

                            <div className="form-group">
                                <div className="col-sm-2 col-sm-10">
                                    <button type="submit" className="btn-lg btn-primary">Upravit profil</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            );
        } else {
            return (<></>);
        }
    }
}

export default UserProfilUpdateComponent;
import React from "react";
import { ApiService } from "../../../services/ApiService";
import { isNull } from "util";

interface Prop {
    handler: any;
    id: number;
    successMes: string;
}

interface State {
    successMes: string,
    user: any,
    id: number,
    loading: boolean
}

class UserProfilDetailComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { successMes: props.successMes, loading: true, user: null, id: props.id };
    }

    componentDidMount() {
        this.loadUser(this.state.id);
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({ loading: true, user: null, successMes: nextProps.successMes });
        this.loadUser(nextProps.id);
    }

    async loadUser(id: number) {
        let path = "user/getById/" + id;
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let u = await response.json();
            this.setState({ user: u, loading: false });
        } if (response.status === 400 || response.status === 404) {
            this.setState({ user: null, loading: false });
        }
    }

    render() {
        if (!this.state.loading && !isNull(this.state.user)) {
            let dateFormatOptions = {day: 'numeric', month: 'numeric', year: 'numeric'};
            let birth = <></>;
            let success = <></>;
            let srcImg = "";
            if (!isNull(this.state.user.Birth)) {
                birth = <>
                    <span className="tital ">Datum narození: </span> {new Date(this.state.user.Birth).toLocaleDateString('cs-CZ', dateFormatOptions)}
                    <div className="bot-border"></div>
                </>
            }
            if (isNull(this.state.user.Image)) {
                srcImg = ApiService.SERVER_PREFIX + "/obrazky/icon/nobody.jpg"
            } else {
                srcImg = ApiService.SERVER_PREFIX + "/uploads/profilePictures/" + this.state.user.Image
            }
            if (this.state.successMes !== "") {
                success = <>
                    <div className="alert alert-success alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"
                            onClick={() => this.setState({ successMes: "" })}>
                            <span aria-hidden="true">x</span>
                        </button>
                        {this.state.successMes}
                    </div>
                </>
            }

            return (
                <div className="container-fluid dashboard-content">
                    {success}
                    <div className="card maxCenterProfil mx-auto">
                        <div className="card-header">
                            <h4 className="text-center">Vítejte na Vašem profilu</h4>
                        </div>
                        <div className="card-body">
                            <div className="text-center">
                                <img alt="User Pic" src={srcImg} className="rounded-circle" width="150" height="150" />
                                <h4 style={{color: '#00b1b1'}}>{this.state.user.Login}</h4>
                                <p>{this.state.user.Role.RoleDescription}</p>
                            </div>
                            <hr />
                            <span className="tital ">Jméno: </span>{this.state.user.Name}
                            <div className="bot-border"></div>
                            <span className="tital ">Příjmení: </span>{this.state.user.Surname}
                            <div className="bot-border"></div>
                            {birth}
                            <span className="tital ">Datum registrace: </span>{new Date(this.state.user.RegistrationDay).toLocaleDateString('cs-CZ', dateFormatOptions)}
                            <div className="bot-border"></div>
                            <span className="tital ">Datum posledního přihlášení: </span>{new Date(this.state.user.LastActivityDay).toLocaleDateString('cs-CZ', dateFormatOptions)}
                        </div>
                    </div>
                </div>
            );
        }
        return (
            <></>
        )
    }
}

export default UserProfilDetailComponent;
import React from "react";
import { FormService } from "../../../services/FormService";
import { ApiService } from "../../../services/ApiService";
import { isNull } from "util";

interface Prop {
    handler: any;
    id: number
}

interface State {
    user: any,
    oldPass: string,
    newPass: string,
    newPassRepeat: string,
    loading: boolean,
    errors: {
        oldPass: string,
        newPass: string,
        newPassRepeat: string
    }
}

class UserProfilChangePasswordComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            user: null,
            oldPass: "",
            newPass: "",
            newPassRepeat: "",
            loading: true,
            errors: {
                oldPass: "",
                newPass: "",
                newPassRepeat: ""
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.loadUser(this.props.id);
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({ loading: true, user: null });
        this.loadUser(nextProps.id);
    }

    async loadUser(id: number) {
        let path = "user/getById/" + id;
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let u = await response.json();
            this.setState({ user: u, loading: false });
        } if (response.status === 400 || response.status === 404) {
            this.setState({ user: null, loading: false });
        }
    }

    handleSubmit(event: any) {
        if (this.validateForm(this.state.errors, this.state)) {
            this.sendData(this.state.user);
        }
        event.preventDefault();
    }

    handleChange(event: any) {
        const newState = FormService.getStateFromEvent(event);
        this.setState(newState);
        this.validateState(event.target.name, event.target.value);
    }

    validateForm(errors: any, states: any) {
        let valid = true;
        Object.keys(errors).forEach(
            (key: string) => {
                this.validateState(key, states[key]);
            }
        );
        Object.values(errors).forEach(
            (val: any) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    validateState(name: any, value: any) {
        let errorsArray = this.state.errors;
        let message: string = '';
        switch (name) {
            case "oldPass":
                if (value.length < 1 || this.hashPassword(value) !== this.state.user.Password){
                    message = 'Heslo uživatele bylo zadáno nesprávně, zkusteto znovu.';
                }
                errorsArray.oldPass = message;
                break;
            case "newPass":
                if (value.length < 1) {
                    message = 'Heslo musíte vyplnit.';
                } else if (value.length < 5) {
                    message = 'Heslo musí obsahovat minimálně pět znaků.';
                }
                errorsArray.newPass = message;
                break;
            case "newPassRepeat":
                if (value.length < 1) {
                    message = 'Toto pole je povinné.';
                } else if (value !== this.state.newPass) {
                    message = 'Hesla se neshodují.';
                }
                errorsArray.newPassRepeat = message;
                break;
            default:
                break;
        }
        this.setState({ errors: errorsArray });
    }
    hashPassword(pass: string) {
        let hashedPass = pass
        //TODO hash
        return hashedPass;
    }

    async sendData(user: any): Promise<boolean> {
        user.Password = this.state.newPass;
        const response = await ApiService.callWithBody("user/update", ApiService.PUT, user);
        if (response.status === 400) {
            console.log("Something is wrong")
        } else if (response.status === 200) {
            this.props.handler("home:Heslo uživatele bylo změněno.");
        }
        return response.ok;
    }

    render() {
        if (!this.state.loading && !isNull(this.state.user)) {
            return (
                <div className="container-fluid dashboard-content">
                    <div className="card maxCenterProfil mx-auto">
                        <div className="card-header">
                            <h4 className="text-center">Formulář pro změnu hesla</h4>
                        </div>
                        <form onSubmit={this.handleSubmit} className="p-4">
                            <div className="form-group">
                                <label>Staré heslo</label>
                                <input autoFocus className="form-control" 
                                placeholder="Zde napiště své staré heslo..." type="password" 
                                name="oldPass" onChange={this.handleChange}/>
                                <span className="field-validation-valid">{this.state.errors.oldPass}</span>
                            </div>
                            <div className="form-group">
                                <label>Nové heslo</label>
                                <input className="form-control" 
                                placeholder="Zde napiště nové heslo..." type="password" 
                                name="newPass" onChange={this.handleChange}/>
                                <span className="field-validation-valid">{this.state.errors.newPass}</span>
                            </div>
                            <div className="form-group">
                                <label>Nové heslo znovu</label>
                                <input className="form-control" 
                                placeholder="Zadaná nová hesla se musí shodovat..." type="password" 
                                name="newPassRepeat" onChange={this.handleChange}/>
                                <span className="field-validation-valid">{this.state.errors.newPassRepeat}</span>
                            </div>
                            <button className="btn-lg btn-primary text-center" type="submit">Změnit heslo</button>
                        </form>
                    </div>
                </div>
            );
        } else {
            return (<></>);
        }
    }
}

export default UserProfilChangePasswordComponent;
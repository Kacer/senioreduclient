import React from "react";
import UserProfilDetailComponent from "./UserProfilDetailComponent";
import UserProfilUpdateComponent from "./UserProfilUpdateComponent";
import UserProfilChangePasswordComponent from "./UserProfilChangePasswordComponent";
import UserProfileTestListComponent from "./UserProfileTestListComponent";

interface Prop {
    parentHandler: any,
    loggedUser: any
}

interface State {
    content: string;
}

class UserProfilComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { content: "home" };
        this.handleChildInfo = this.handleChildInfo.bind(this);
    }

    renderBody(page: string) {
        let message = "";
        if(page.includes(":")){
            message = page.substr(page.indexOf(":")+1);
            page =  page.substr(0, page.indexOf(':')); 
        }
        switch (page) {
            case "home":
                return (
                    <UserProfilDetailComponent id={this.props.loggedUser.Id} handler={this.handleChildInfo} successMes={message} />
                )
            case "updateProfil":
                return (
                    <UserProfilUpdateComponent id={this.props.loggedUser.Id} handler={this.handleChildInfo} />
                )
            case "updatePassword":
                return (
                    <UserProfilChangePasswordComponent id={this.props.loggedUser.Id} handler={this.handleChildInfo}  />
                )
            case "myTests":
                return (
                    <UserProfileTestListComponent />
                )
            default:
                return (
                    <></>
                )
        }
    }

    handleChildInfo(info: string, top: boolean) {
        if (top) {
            this.props.parentHandler(info)
        } else {
            this.setState({ content: info });
        }
    }

    render() {
        return (
            <>
                <div className="nav-left-sidebar sidebar-dark">
                    <nav className="navbar navbar-expand-md navbar-light w-100">
                        <ul className="navbar-nav flex-column">
                            <li className="nav-divider">
                                <h5>Profil uživatele</h5>
                            </li>
                            <li className="nav-item">
                                <ul className="nav flex-column">
                                    <li className="nav-item">
                                        <a className="nav-link" href="/User"
                                            onClick={(e) => {
                                                e.preventDefault();
                                                this.setState({ content: "home" })
                                            }
                                            }>Profil</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="/User"
                                            onClick={(e) => {
                                                e.preventDefault();
                                                this.setState({ content: "updateProfil" })
                                            }
                                            }>Upravit profil</a>
                                    </li>
                                    <li className="nav-item" style={{display: "none"}}>
                                        <a className="nav-link" href="/User"
                                            onClick={(e) => {
                                                e.preventDefault();
                                                this.setState({ content: "updatePassword" })
                                            }
                                            }>Upravit heslo</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="/User"
                                            onClick={(e) => {
                                                e.preventDefault();
                                                this.setState({ content: "myTests" })
                                            }
                                            }>Moje testy</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div className="dashboard-wrapper">
                    {this.renderBody(this.state.content)}
                </div>
            </>
        );
    }
}

export default UserProfilComponent;



import React from "react";
import HelloComponent from "../../HelloComponent";
import UserVocabularyTableComponent from "./UserVocabularyTableComponent";

interface Prop {
    
}

interface State {
    
}

class UserVocabularyComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
    }

    render() {
        return (
            <>
                <div className="nav-left-sidebar sidebar-dark">
                    <nav className="navbar navbar-expand-md navbar-light w-100">
                        <ul className="navbar-nav flex-column">
                            <li className="nav-divider">
                                <h5>Slovníček pojmů</h5>
                            </li>
                            <li className="nav-item">
                                <p>Vítejte ve slovníčku pojmů.</p>
                                <p>Zde si můžete vyhledat a přečíst slovíčka, kterým v textu nerozumíte.</p>
                                <p>Naleznete zde slovíčka, která jsou v textu označena jiným typem písma.</p>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div className="dashboard-wrapper">
                    <UserVocabularyTableComponent/>
                </div>
            </>
        );
    }
}

export default UserVocabularyComponent;
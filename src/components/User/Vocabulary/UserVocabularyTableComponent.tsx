import React from "react";
import { ApiService } from "../../../services/ApiService";

interface Prop {
    
}

interface State {
    listWord: Array<any>,
    loading: boolean
}

class UserVocabularyTableComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { loading: true, listWord: [] };
    }

    componentDidMount() {
        this.loadWords();
    }

    async loadWords() {
        let path = "vocabulary/getAll";
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let words = await response.json();
            this.setState({ listWord: words, loading: false });
        } if (response.status === 401) {
            this.setState({ listWord: [], loading: false });
        }
    }

    renderItems() {
        const items: any = [];
        if (!this.state.loading) {
            this.state.listWord.forEach((word: any) => {
                items.push(
                    <tr key={word.Id}>
                        <td><strong>{word.Word}</strong></td>
                        <td>{word.Description}</td>
                    </tr>
                )
            });
        }
        return (<>
            <thead className="thead-dark">
                <tr>
                    <th scope="col">Slovíčko</th>
                    <th scope="col">Význam</th>
                </tr>
            </thead>
            <tbody>
                {items}
            </tbody>
        </>

        )

    }

    render() {
        return (
            <div className="container-fluid dashboard-content">
                <table className="table">
                    {this.renderItems()}
                </table>
            </div>

        );
    }
}

export default UserVocabularyTableComponent;
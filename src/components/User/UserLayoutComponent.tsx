import React from "react";
import UserHomeComponent from "./Home/UserHomeComponent";
import { Link, Redirect } from "react-router-dom";
import UserProfilComponent from "./Profil/UserProfilComponent";
import UserTextComponent from "./Text/UserTextComponent";
import UserVocabularyComponent from "./Vocabulary/UserVocabularyComponent";
import UserMessageComponent from "./Message/UserMessageComponent";
import { ApiService } from "../../services/ApiService";
import { UserService } from "../../services/UserService";
import  "../../css/profil.css";
import UserTestFillingComponent from "./Test/UserTestFillingComponent";

interface Prop {

}

interface State {
    switchComp: string,
    loggedUser: any,
    loading: boolean
}

class UserLayoutComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { switchComp: "home", loggedUser: '', loading: true }
        document.title = "Výukové prostředí";
        this.handleChildInfo = this.handleChildInfo.bind(this);
    }

    componentDidMount() {
        this.updateLoggedUser();
    }

    async updateLoggedUser() {
        const response = await ApiService.call("user/getLoggedUser", ApiService.GET);
        if (response.ok) {
            response.json()
                .then((user) => {
                    this.setState({ loggedUser: user, loading: false });
                });
        } else {
            this.setState({ loggedUser: "", loading: false });
        }
    }

    handleChildInfo(page: string) {
        this.setState({ switchComp: page });
    }

    renderBody(page: string) {
        let id = -1;
        if(page.includes(":")){
            id = parseInt(page.substr(page.indexOf(":")+1));
            page =  page.substr(0, page.indexOf(':')); 
        }

        switch (page) {
            case "home":
                return (
                    <UserHomeComponent parentHandler={this.handleChildInfo} />
                )
            case "profil":
                return (
                    <UserProfilComponent parentHandler={this.handleChildInfo} loggedUser={this.state.loggedUser} />
                )
            case "text":
                return (
                    <UserTextComponent parentHandler={this.handleChildInfo} />
                )
            case "slovnik":
                return (
                    <UserVocabularyComponent />
                )
            case "napis":
                return (
                    <UserMessageComponent parentHandler={this.handleChildInfo} />
                )
            case "test-filling":
                return (
                    <UserTestFillingComponent parentHandler={this.handleChildInfo} testId={id} />
                )
            default:
                return (
                    <></>
                )
        }
    }

    render() {
        let user = this.state.loggedUser;
        if(this.state.loading){
            return<></>;
        }
        if (user == '') {
            return <Redirect to="/login" />
        }

        let showAdminButtonStyle = {
            display: 'none'
        };
        if(user.Role.Identificator === 'admin') {
            showAdminButtonStyle = {
                display: 'flex'
            };
        }

        return (
            <div className="dashboard-main-wrapper">
                <div className="dashboard-header">
                    <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
                        <a className="nav-link mb-1" href="/User"
                            onClick={(e) => { 
                                e.preventDefault(); 
                                this.setState({ switchComp: "home" })
                            }}>Hlavní stránka</a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse ml-3 mb-1" id="navbarCollapse">
                            <ul className="navbar-nav mr-auto">

                                <ul className="navbar-nav mr-auto">
                                    <li className="nav-item active">
                                        <a className="nav-link" href="Admin"
                                            onClick={(e) => { 
                                                e.preventDefault(); 
                                                this.setState({ switchComp: "profil" }) 
                                            }}>Profil</a>
                                    </li>
                                    <li className="nav-item active">
                                        <a className="nav-link" href="Admin"
                                            onClick={(e) => { 
                                                e.preventDefault(); 
                                                this.setState({ switchComp: "text" }) 
                                                }}>Učební texty</a>
                                    </li>
                                    <li className="nav-item active">
                                        <a className="nav-link" href="Admin"
                                            onClick={(e) => { 
                                                e.preventDefault(); 
                                                this.setState({ switchComp: "slovnik" }) 
                                                }}>Slovníček</a>
                                    </li>
                                    <li className="nav-item active">
                                        <a className="nav-link" href="Admin"
                                            onClick={(e) => { 
                                                e.preventDefault(); 
                                                this.setState({ switchComp: "napis" }) 
                                                }}>Napište nám</a>
                                    </li>
                                </ul>
                            </ul>
                            <div className="form-inline mt-2 mt-md-0" style={showAdminButtonStyle}>
                                <Link className="btn btn-outline-primary my-2 my-sm-0 mr-1" to="/Admin">Administrátorský panel</Link>
                            </div>
                            <div className="form-inline mt-2 mt-md-0">
                                <a className="btn btn-outline-primary my-2 my-sm-0" href="/" onClick={
                                    (e) => {
                                        UserService.logOut();
                                    }}>Odhlášení</a>
                            </div>
                        </div>
                    </nav>

                </div>
                <div id="dynamicContent">
                    {this.renderBody(this.state.switchComp)}
                </div>
            </div>
        );
    }
}

export default UserLayoutComponent;
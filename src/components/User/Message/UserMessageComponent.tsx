import React from "react";
import UserMessageCreateComponent from "./UserMessageCreateComponent";

interface Prop {
    parentHandler: any;
}

interface State {
    content: string;
}

class UserMessageComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { content: "home" };
        this.handleChildInfo = this.handleChildInfo.bind(this);
    }


    handleChildInfo(info: string, top: boolean) {
        if (top) {
            this.props.parentHandler(info)
        } else {
            this.setState({ content: info });
        }
    }

    render() {
        return (
            <>
                <div className="nav-left-sidebar sidebar-dark">
                    <nav className="navbar navbar-expand-md navbar-light w-100">
                        <ul className="navbar-nav flex-column">
                            <li className="nav-divider">
                                <h5>Jak napsat zprávu</h5>
                            </li>
                            <li className="nav-item">
                                <p>Zde můžete kontaktovat naše administrátory s případným dotazem nebo připomínkou.</p>
                                <p>
                                    Abyste mohli kontaktovat naše administrátory, potřebujete mít platný e-mail, aby vás mohli zpětně kontaktovat.
                    Pokud e-mail doposud nemáte, klikněte na tlačítko <b>Informace o e-mailu</b>, které Vás přesune na články, kde si
                    můžete vyhledat, jak si takový e-mail vytvořit a pracovat s ním.
                </p>
                            </li>
                            <a className="btn btn-primary" href="/User" onClick={(e) => {
                                e.preventDefault(); 
                                this.props.parentHandler("text")}
                                }>Informace o e-mailu</a>
                        </ul>
                    </nav>
                </div>
                <div className="dashboard-wrapper">
                    <UserMessageCreateComponent handler={this.handleChildInfo} />
                </div>
            </>
        );
    }
}

export default UserMessageComponent;
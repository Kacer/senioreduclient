import React from "react";
import { FormService } from "../../../services/FormService";
import { ApiService } from "../../../services/ApiService";

interface Prop {
    handler: any;
}

interface State {
    email: string,
    subject: string,
    text: string,
    errors: {
        email: string,
        text: string
    },
    success: boolean;
}

class UserMessageCreateComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            email: "",
            subject: "",
            text: "",
            errors: {
                email: "",
                text: ""
            },
            success: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event: any) {
        if (this.validateForm(this.state.errors, this.state)) {
            this.sendData();
        }
        event.preventDefault();
    }

    handleChange(event: any) {
        const newState = FormService.getStateFromEvent(event);
        this.setState(newState);
        this.validateState(event.target.name, event.target.value);
    }

    validateForm(errors: any, states: any) {
        let valid = true;
        Object.keys(errors).forEach(
            (key: string) => {
                this.validateState(key, states[key]);
            }
        );
        Object.values(errors).forEach(
            (val: any) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    validateState(name: any, value: any) {
        let errorsArray = this.state.errors;
        switch (name) {
            case "email":
                errorsArray.email = value.length < 1 ? 'Váš e-mail je vyžadován.' : '';
                break;
            case "text":
                errorsArray.text = value.length < 1 ? 'Text nemůže být prázdný.' : '';
                break;
            default:
                break;
        }
        this.setState({ errors: errorsArray });
    }

    async sendData(): Promise<boolean> {
        var jsonData = {
            sender: this.state.email,
            subject: this.state.subject,
            text: this.state.text
        };
        const response = await ApiService.callWithBody("message/send", ApiService.POST, jsonData);
        if (response.status === 400) {
            console.log("Something is wrong")
        } else if (response.status === 200) {
            this.setState({
                success: true, 
                email: "",
                subject: "",
                text: "",
                errors: {
                    email: "",
                    text: ""
                }
            });
        }
        return response.ok;
    }

    handeInfo() {
        if (this.state.success) {
            return (
                <div className="alert alert-success alert-dismissible" role="alert">
                    <button type="button" className="close" aria-label="Close"
                    onClick={() => this.setState({success: false})}><span aria-hidden="true">x</span></button>
                    Vaše zpráva byla úspěšně odeslána.
                </div>
            );
        }
    }

    render() {
        return (
            <div className="container-fluid dashboard-content">
                <form onSubmit={this.handleSubmit}>
                    {this.handeInfo()}
                    <h2>Odeslat zprávu</h2>
                    <div className="container">
                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Váš e-mail</label>
                            <div className="col-sm-10">
                                <input className="form-control text-box single-line" type="email" name="email" value={this.state.email} onChange={this.handleChange} />
                                <span className="field-validation-valid">{this.state.errors.email}</span>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Předmět</label>
                            <div className="col-sm-10">
                                <input className="form-control" type="text" name="subject" value={this.state.subject} onChange={this.handleChange} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Zpráva</label>
                            <div className="col-sm-10 float-right">
                                <textarea className="form-control" cols={20} rows={2} name="text" value={this.state.text} onChange={this.handleChange} ></textarea>
                                <span className="field-validation-valid">{this.state.errors.text}</span>
                            </div>
                        </div>

                        <div className="form-group float-right">
                            <div className="col-sm-2 col-sm-10">
                                <button type="submit" className="btn btn-primary">Odeslat</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        );
    }
}

export default UserMessageCreateComponent;
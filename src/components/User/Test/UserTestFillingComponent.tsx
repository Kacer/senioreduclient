import React from "react";
import { ApiService } from "../../../services/ApiService";
import { response } from "express";

interface Prop {
    testId: number;
    parentHandler: any;
}

interface State {
    loading: boolean;
    showingCorrectAnswers: boolean;
    test: any;
    submittedTest: any;
}

class UserTestFillingComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            loading: true,
            showingCorrectAnswers: false,
            test: {},
            submittedTest: {}
        }

        this.handleChildInfo = this.handleChildInfo.bind(this);
        this.handleSubmitTest = this.handleSubmitTest.bind(this);
        this.handleAnswerChange = this.handleAnswerChange.bind(this);
    }

    fetchTest(testId: number) {
        ApiService.call('test/begin/' + testId, ApiService.POST)
            .then(response => {
                if(response.ok) {
                    response.json()
                        .then(test => {
                            this.setState({test: test, loading: false});
                        });
                } else {
                    this.setState({loading: false});
                }
            });
    }

    componentDidMount() {
        this.fetchTest(this.props.testId);
    }

    handleChildInfo(page: string) {
        this.props.parentHandler(page);
    }

    handleSubmitTest(e: any) {
        e.preventDefault();
        let unansweredQuestions: any = this.getUnansweredQuestions();
        if(unansweredQuestions.length !== 0) {
            let res: boolean = window.confirm("V testu jsou nevyplněné otázky. Opravdu si přejete test odevzdat?");
            if(!res) {
                return;
            }
        }

        this.submitTest();
    }

    handleAnswerChange(e: any) {
        let answer: any = this.findAnswerById(e.target.name);
        if(answer != null) {
            answer.isSelected = e.target.checked;
            this.setState({});
        }
    }

    findAnswerById(id: string): any {
        let answer: any = null;

        this.state.test.Questions.forEach((q: any) => {
            q.Answers.forEach((a: any) => {
                if(a.Id == id) {
                    answer = a;
                }
            });
        });

        return answer;
    }

    getUnansweredQuestions(): any {
        let questions: any = [];

        this.state.test.Questions.forEach((q: any) => {
            let filledAnswerCount: number = 0;

            q.Answers.forEach((a: any) => {
                if(a.isSelected) {
                    filledAnswerCount++;
                }
            });

            if(filledAnswerCount === 0) {
                questions.push(q);
            }
        });

        return questions;
    }

    submitTest() {
        ApiService.callWithBody('test/submit', ApiService.POST, this.state.test)
            .then(response => {
                if(response.ok) {
                    response.json()
                        .then(submittedTest => {
                            this.setState({submittedTest: submittedTest, showingCorrectAnswers: true})
                        }
                    );
                } else {
                    alert('Nepodařilo se test odevzdat. Zkontrolujte si své internetové připojení nebo zopakujte odevzdání testu později.')
                }
            }
        );
    } 

    renderTestTofill() {
        let questions: any = [];
        let questionCounter = 1;
        this.state.test.Questions.forEach((q: any) => {
            let className: string = questionCounter > 1 ? 'card-body border-top' : 'card-body';

            let answerCounter = 1;
            let answers: any = [];
            q.Answers.forEach((a: any) => {
                answers.push(
                    <div key={a.Id} className="form-check">
                        <label className="form-check-label">
                            <input type="checkbox" name={a.Id} checked={a.isSelected} onChange={this.handleAnswerChange} />&nbsp;
                            {answerCounter++}. {a.Answer}
                        </label>
                    </div>
                );
            });

            questions.push(
                <div key={q.Id} className={className}>
                    <h5>{questionCounter++}. otázka:</h5>
                    <h6>{q.Question}</h6>
                    <div></div>
                    <h5>Odpovědi: </h5>
                    {answers}
                </div>
            );
        });

        return (
            <>
                <div className="nav-left-sidebar sidebar-dark">
                    <nav className="navbar navbar-expand-md navbar-light w-100">
                        <ul className="navbar-nav flex-column">
                            <li className="nav-divider">
                                <h5>Test</h5>
                            </li>
                            <li className="nav-item">
                                <p>Zde je Test samotný. Máte libovolné množství času k vyplnění testu.</p>
                                <p>Každá otázka má jednu a více správných odpovědí. Za každou správnou otázku získáte jeden bod.</p>
                                <p>Pokud odejdete z této stránky, test se Vám neuloží a ani nevyhodnotí.</p>
                                <p>Po stisknutí tlačítka - <b>Ukončit test</b> se Vám ukáže vyhodnocení testu.</p>
                            </li>
                            <a className="btn btn-outline-primary" href="/User" onClick={e => {
                                e.preventDefault();
                                this.handleChildInfo("text");
                            }}>Zpět na všechny čl&#225;nky</a>
                        </ul>
                    </nav>
                </div>

                <div className="dashboard-wrapper">
                    <div className="container-fluid dashboard-content">
                        <form className="col-12" onSubmit={this.handleSubmitTest}>
                            <div className="card">
                                <div className="card-header">{this.state.test.ArticleName}</div>
                                {questions}
                            </div>
                            <button type="submit" className="btn btn-primary">Ukončit test</button>
                        </form>
                    </div>
                </div>
            </>
        );
    }

    renderEvaluatedTest() {
        let testFinished: any = this.state.submittedTest.TestFinished;
        let testSubmitted: any = this.state.submittedTest.TestSubmitted;
        console.log(testSubmitted);

        let questions: any = [];
        let questionCounter = 1;
        testSubmitted.Questions.forEach((q: any) => {
            let className: string = questionCounter > 1 ? 'card-body border-top' : 'card-body';

            let answerCounter = 1;
            let answers: any = [];
            q.Answers.forEach((a: any) => {
                let style: any = {
                    color: a.WasCorrect ? (a.IsCorrect ? 'lime' : '') : 'red'
                };
                answers.push(
                    <div key={a.Id} className="form-check">
                        <label className="form-check-label" style={style}>
                            <input type="checkbox" name={a.Id} checked={a.isSelected} disabled />&nbsp;
                            {answerCounter++}. {a.Answer}
                        </label>
                    </div>
                );
            });

            questions.push(
                <div key={q.Id} className={className}>
                    <h5>{questionCounter++}. otázka:</h5>
                    <h6>{q.Question}</h6>
                    <div></div>
                    <h5>Odpovědi: </h5>
                    {answers}
                </div>
            );
        });

        return (
            <>
                <div className="nav-left-sidebar sidebar-dark">
                    <nav className="navbar navbar-expand-md navbar-light w-100">
                        <ul className="navbar-nav flex-column">
                            <li className="nav-divider">
                                <h5>Vyhodnocení Testu</h5>
                            </li>
                            <li className="nav-item">
                                <p>Na pravo vidíte vyhodnocení Vašeho Testu.</p>
                                <p>Úplně nahoře můžete vidět, kolik bodů jste z Testu získal/a.</p>
                                <p>Zeleně označené odpovědi jsou správně a získáváte za ně bod.</p>
                                <p>Červeně označené odpovědi, které jsou zleva označeny <i>"fajfkou"</i> jste zodpovědel/a špatně.</p>
                                <p>Červené odpovědi, které zleva nejsou zatržené <i>"fajfkou"</i> jsou správně, ale Vy jste je nezatrhl/a.</p>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div className="dashboard-wrapper">
                    <div className="container-fluid dashboard-content">
                        <div className="card">
                            <div className="card-header">{testSubmitted.ArticleName}<p><b>Získali jste {testFinished.Score} bodů z {testFinished.MaxScore} bodů </b></p></div>
                            {questions}
                        </div>
                        <a className="btn btn-outline-primary" href="/User" onClick={e => {
                                e.preventDefault();
                                this.handleChildInfo("text");
                            }}>Zpět na všechny čl&#225;nky</a>
                    </div>
                </div>
            </>
        );
    }

    render() {
        if(this.state.loading) {
            return <></>;
        }

        if(this.state.showingCorrectAnswers) {
            return this.renderEvaluatedTest();
        }

        return this.renderTestTofill();
    }
}

export default UserTestFillingComponent;
import React from "react";
import '../../../css/formLoginRegister.css';
import { Link, Redirect } from "react-router-dom";
import { ApiService } from "../../../services/ApiService";

interface Prop {
    // empty
}

interface State {
    name: string;
    surname: string;
    username: string;
    pass: string;
    passRepeat: string;
    errors: {
        name: string,
        surname: string
        username: string,
        pass: string,
        passRepeat: string
    };
    singin: boolean;
}

class RegisterComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            name: '',
            surname: '',
            username: '',
            pass: '',
            passRepeat: '',
            errors: {
                name: '',
                surname: '',
                username: '',
                pass: '',
                passRepeat: '',
            },
            singin: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        document.title = "Registrace";
    }

    validateForm(errors: any, states: any) {
        let valid = true;
        Object.keys(errors).forEach(
            (key: string) => {
                this.validateState(key, states[key]);
            }
        );
        Object.values(errors).forEach(
            (val: any) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    handleSubmit(event: any) {
        if (this.validateForm(this.state.errors, this.state)) {
            this.sendData();
        }
        event.preventDefault();
    }

    handleChange(event: any) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        const newState = { [name]: value } as Pick<State, keyof State>;
        this.setState(newState);
        this.validateState(name, value);
    }

    validateState(name: any, value: any) {
        let errorsArray = this.state.errors;
        let message: string = '';
        switch (name) {
            case "name":
                errorsArray.name = value.length < 1 ? 'Vaše jméno je vyžadováno.' : '';
                break;
            case "surname":
                errorsArray.surname = value.length < 1 ? 'Vaše příjmení je vyžadováno.' : '';
                break;
            case "username":
                errorsArray.username = value.length < 1 ? 'Vaši přezdívku musíte vyplnit.' : '';
                break;
            case "pass":
                if (value.length < 1) {
                    message = 'Heslo musíte vyplnit.';
                } else if (value.length < 5) {
                    message = 'Heslo musí obsahovat minimálně pět znaků.';
                }
                errorsArray.pass = message;
                break;
            case "passRepeat":
                if (value.length < 1) {
                    message = 'Toto pole je povinné.';
                } else if (value !== this.state.pass) {
                    message = 'Hesla se neshodují.';
                }
                errorsArray.passRepeat = message;
                break;
            default:
                break;
        }
        this.setState({ errors: errorsArray });
    }

    async sendData(): Promise<boolean> {
        try {
            var jsonData = {
                name: this.state.name,
                surname: this.state.surname,
                login: this.state.username,
                password: this.state.pass,
                passwordRepeat: this.state.passRepeat
            };
            const response = await ApiService.callWithBody("RegisterApi/register", ApiService.POST, jsonData);
            if (response.status === 400) {
                let responseBody: any;
                responseBody = await response.json();
                let errorsArray = this.state.errors;
                errorsArray.username = responseBody["Message"];
                this.setState({ errors: errorsArray });
            } else if (response.status === 201) {
                this.setState({ singin: true });
            }
            return response.ok;
        } catch (ex) {
            return false;
        }
    }

    render() {
        const singin = this.state.singin;
        if (singin === true) {
            return <Redirect to={"/User"} />
        }
        return (
            <>
                <div className="float-right m-4">
                    <Link className="btn btn-primary" to="/">Domů</Link>
                    <Link className="btn btn-primary" to="/Login">Příhlášení</Link>
                </div>

                <div className="text-center">
                    <form onSubmit={this.handleSubmit} className="form-signin">
                        <img className="mb-4" src="/images/icon/registration.png" alt="register" width="90" height="90" />
                        <h3>Prosím, zde se můžete zaregistrovat. <small>Vše je zdarma!</small></h3>

                        <div className="form-group">
                            <input className="form-control" type="text" name="name" value={this.state.name} placeholder="Jméno" onChange={this.handleChange} />
                            <span className="field-validation-error" >{this.state.errors.name}</span>
                            <input className="form-control" type="text" name="surname" value={this.state.surname} placeholder="Příjmení" onChange={this.handleChange} />
                            <span className="field-validation-error" >{this.state.errors.surname}</span>
                            <input className="form-control" type="text" name="username" value={this.state.username} placeholder="Uživatelské jméno" onChange={this.handleChange} />
                            <span className="field-validation-error" >{this.state.errors.username}</span>
                        </div>

                        <div className="row">
                            <div className="col-sm-6 col-md-6">
                                <div className="form-group">
                                    <input className="form-control" type="password" name="pass" value={this.state.pass} placeholder="Heslo" onChange={this.handleChange} />
                                    <span className="field-validation-error" >{this.state.errors.pass}</span>
                                </div>
                            </div>
                            <div className="col-sm-6 col-md-6">
                                <div className="form-group">
                                    <input className="form-control" type="password" name="passRepeat" value={this.state.passRepeat} placeholder="Heslo znovu" onChange={this.handleChange} />
                                    <span className="field-validation-error" >{this.state.errors.passRepeat}</span>
                                </div>
                            </div>
                        </div>

                        <button className="btn btn-lg btn-primary btn-block" type="submit">Zaregistrovat se</button>
                        <p className="mt-5 mb-3 text-muted">&copy; 2020</p>
                    </form>
                </div>
            </>
        );
    }
}

export default RegisterComponent;
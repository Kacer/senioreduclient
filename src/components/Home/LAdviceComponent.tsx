import React from "react";
import { Link } from "react-router-dom";

interface Prop {
    handler:any 
}

interface State {
    // empty
}

class LAdviceComponent extends React.Component<Prop, State> {
    render() {
        return (
            <>
                <h2>Jak se mám přihlásit?</h2>
                <h3>Pokud jste se nikdy nepřihlašoval/a nebojte se, projdeme si to teď krok za krokem.</h3>
                <p><strong>A co vlastně budu z přihlášení mít?</strong></p>
                <p>
                    Když jste přihlášení, máte přístup k výukovým kurzům a testům, které si můžete vyzkoušet, takže neváhejte a přihlašte se.
                    Pokud jste na této stránce poprvé a ještě jste se nikdy neregistroval/a, musíte se nejdřív zaregistrovat. Jak na to, se dozvíte, pokud stisknete
    tlačítko - <strong>Jak se zaregistrovat</strong>
                </p>
                <Link className="btn btn-primary" to="/" onClick={()=>this.props.handler('r')}>Jak se zaregistrovat</Link>
                <br />
                <br />
                <p><strong>Krok 1</strong></p>
                <img src="/images/uvod/text/login_button.jpg" className="img-fluid" alt="Tlačítko přihlásit se" />
                <br />
                <p>
                    Až přístě příjdete na tuto stránku, už se nemusíte dívat na tento návod. Stačí, když hned nahoře na této stránce
    v pravo stisknete tlačítko <strong>Přihlásit se</strong>. Pro zvýraznění je na obrázku toto tlačítko v zeleném rámečku. Přihlásíte se tak,
    jak si to nyní ukážeme v následujích krocích.
</p>
                <p><strong>Krok 2</strong></p>
                <img src="/images/uvod/text/login-formular.png" className="img-fluid" alt="Formulář přihlášení" />
                <br />
                <p>
                    Po stisknutí tlačítka <i>Přihlásit se</i> se Vám zobrazí toto <i>"okno"</i>. Pro lepší viditelnost jsme zde opět
    použili barevné rámečky. Do červeného políčka vyplníte Vaši přezdívku, kterou jste si vymysleli při registraci.
    Dávejte si pozor na velká a malá písmenka. Přezdívka se musí přesně shodovat s tou, kterou jste si před tím zaregistrovali.
</p>
                <p>
                    Do zeleného políčka vyplníte heslo, které jste si vymysleli při registraci. Opět si musíte dávat pozor, aby jste
                    napsali přesně stejné heslo, které jste si zaregostrovali.
</p>
                <p>
                    Když máte obě políčka vyplněná, zmáčkněte modré tláčítko pod nimi - <strong>Přihlásit se</strong> - pokud se
    vše povedlo tak, jak mělo, načte se Vám nové <i>okno</i>, kde již můžete prohlíže učební texty a vyplňovat testy.
</p>
                <br />
                <p><strong>Krok 3</strong></p>
                <img src="/images/uvod/text/login_error.jpg" className="img-fluid" alt="Error hláška" />
                <br />
                <p>
                    Text v zeleném rámečku se Vám ukáže, pokud jste zadal/a svoji přezdívku nebo heslo nesprávně. Zkuste to znovu
    a pomaleji, ať Vám to tentokrát výjde. Znovu postupujte podle <strong>Kroku 2</strong>
                </p>
                <br />
                <p><strong>A co když se mi stejně nepovede přihlásit?</strong></p>
                <p>
                    Pokud jste si jistý/á, že jste se před přihlášením nejprve zaregistroval/a a přesto Vám nejde se přihlásit, budete
                    se bohužel muset zaregistrovat znovu. Z hlediska ochrany osobních údajů je Vaše heslo zašifrované, a proto Vám ho
                    nemůžeme poskytnout pro další přihlášení. Ale nebojte, nic tím neztratíte, učební texty budou stejné pořád.
</p>
                <p><strong>Nyní prosím klikněte na modré tlačítko - Jít se přihlásit</strong></p>
                <div className="form-group">
                    <div className="col-sm-2 col-sm-10">
                        <Link href="/Login" className="btn btn-primary pull-right" to="/Login">Jít se přihlásit</Link>
                        <Link className="btn btn-primary" to="/" onClick={()=>this.props.handler('h')}>Zpět na hlavní stránku</Link>
                    </div>
                </div>
            </>
        );
    }
}

export default LAdviceComponent;
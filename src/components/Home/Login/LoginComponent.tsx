import React from "react";
import { Link, Redirect } from "react-router-dom";
import { ApiService } from "../../../services/ApiService";

interface Prop {

}

interface State {
    login: string,
    password: string,
    error: string,
    loggedUser: any,
    loading: boolean
}

class LoginComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = {
            login: '',
            password: '',
            error: '',
            loggedUser: null,
            loading: true
        };
        document.title = "Přihlášení";
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.updateLoggedUser();
    }

    async updateLoggedUser() {
        ApiService.call("user/getLoggedUser", ApiService.GET)
            .then(response => {
                if(response.ok) {
                    response.json()
                        .then(user => {
                            this.setState({loggedUser: user, loading: false});
                        });
                } else {    // in all other cases we do not have the user
                    this.setState({loggedUser: null, loading: false});
                }
            }
        );
    }

    handleSubmit(event: any) {
        event.preventDefault();

        const { login, password } = this.state;

        // stop here if form is invalid
        if (!(login && password)) {
            return;
        }
        this.login();
    }

    async login(): Promise<boolean> {
        try {
            this.setState({loading: true});

            var jsonData = {
                login: this.state.login,
                password: this.state.password
            };
            const response = await ApiService.callWithBody("LoginApi/LogInInsomnia", ApiService.POST, jsonData);
            let user = await response.json();
            if(response.ok) {
                this.setState({loggedUser: user, loading: false});
            } else {
                let error = this.state.error;
                error = user["Message"];
                this.setState({ error: error, loading: false });
            }
            
            return response.ok;
        } catch (ex) {
            console.log(ex);
            return false;
        }
    }

    handleChange(event: any) {
        const { name, value } = event.target;
        const newState = { [name]: value } as Pick<State, keyof State>;
        this.setState(newState);
    }


    render() {
        if(this.state.loading) {
            return <></>;
        }
        const { loggedUser, login, password, error } = this.state;
        if (loggedUser != null) {
            if(loggedUser.Role.Identificator === "admin"){
                return <Redirect to={"/Admin"} />
            }else{
                return <Redirect to={"/User"} />
            } 
        }
        return (
            <>
                <div className="float-right m-4">
                    <Link className="btn btn-primary" to="/">Domů</Link>
                    <Link className="btn btn-primary" to="/Register">Registace</Link>
                </div>

                <div className="text-center">

                    <form onSubmit={this.handleSubmit} className="form-signin">
                        {error &&
                            <div className={'alert alert-danger alert-dismissible'} role="alert">{error}</div>
                        }
                        <img className="mb-4" src="/images/icon/login.png" alt="login" width="110" height="165" />
                        <h1 className="h3 mb-3 font-weight-normal">Prosím přihlašte se</h1>
                        <label htmlFor="inputEmail" className="sr-only">Přezdívka</label>
                        <input type="text" id="inputEmail" className="form-control" placeholder="Přezdívka"
                            name="login" value={login} onChange={this.handleChange} />
                        <label htmlFor="inputPassword" className="sr-only">Heslo</label>
                        <input type="password" id="inputPassword" className="form-control" placeholder="Heslo"
                            name="password" value={password} onChange={this.handleChange} />
                        <button className="btn btn-lg btn-primary btn-block" type="submit">Přihlásit se</button>
                        <p className="mt-5 mb-3 text-muted">© 2020</p>
                    </form>    </div>
            </>
        );
    }
}

export default LoginComponent;
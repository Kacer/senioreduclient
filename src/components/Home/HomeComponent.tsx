import React from "react";

interface Prop {
    //empty
}

interface State {
    // empty
}

class HomeComponent extends React.Component<Prop, State> {

    private divStyle = {
        width: "40rem",
    };

    render() {
        return (
            <>
                <h1>Vítejte na výukové stránce pro seniory</h1>
                <h2>Na této stránce se dozvíte, jak zacházet s počítačem a s Internetem.</h2>
                <p><strong>Vítejte na naší stránce!</strong> </p>
                <p>
                    Pokud nevíte, jak se příhlásit, <i>klikněte</i> prosím na tlačítko <strong>Jak se přihlásit</strong>, které najdete níže. Objeví se
    Vám stránka, kde bude popsáno, jak se přihlásit.
</p>
                <div className="card" style={this.divStyle}>
                    <img className="card-img-top" src="/images/uvod/text/login.jpg" alt="Login" />
                    <div className="card-body">
                        <h5 className="card-title">Příhlášení</h5>
                        <p className="card-text">Pokud nevíte, jak se přihlásit, klikněte na tlačítko pod tímto textem, nebo hledejte obrázek, jako je tento.</p>

                    </div>
                </div>
                <br />
                <p>
                    Pokud jste na této stránce poprvé, musíte se nejdříve zaregistrovat. Po registraci budete mít přístup k množství informací,
                    které Vám objasní práci s počítačem a Internetem. K dispozici jsou zde výukové texty, ve který si můžete nastudovat
                    konkrétní problematiku, jejíž zvládnutí si následně můžete vyzkoušet v testu, který jsme pro Vás připravili.
</p>
                <p>Nebojte se, registrací se k ničemu nezavazujete a Vaše údaje nebude nikdo zneužívat.</p>
                <div className="card" style={this.divStyle}>
                    <img className="card-img-top" src="/images/uvod/text/register.jpg" alt="Register" />
                    <div className="card-body">
                        <h5 className="card-title">Registrace</h5>
                        <p className="card-text">Pokud nevíte, jak se zaregistrovat, klikněte na tlačítko pod tímto textem, nebo hledejte obrázek, jako je tento.</p>

                    </div>
                </div>
                <br />
                <p>
                    Děkujeme, že jste zavítali na naše stránky a doufáme, že se Vám budou líbit a budou pro Vás užitečným průvodcem při
                    práci s počítačem a Internetem
</p>
                <p><strong>Přejeme hezký den!</strong></p>
            </>
        );
    }
}

export default HomeComponent;
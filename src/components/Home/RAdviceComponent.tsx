import React from "react";
import { Link } from "react-router-dom";

interface Prop {
    handler:any;
}

interface State {
    // empty
}

class RAdviceComponent extends React.Component<Prop, State> {
    render() {
        return (
            <>
            <h2>Jak se mám zaregistrovat?</h2>
                <h3>Pokud jste se nikdy neregistroval/a nebojte se, projdeme si to teď krok za krokem.</h3>
                <p><strong>A co vlastně budu z registrace mít?</strong></p>
                <p>
                    Díky registraci budete mít na této stránce uživatelský profil s Vašimi údaji. Až na naši stránku zavítáte příště,
                    budete se moci jen přihlásit a díky tomu máte přístup k výukovým textům a testům, které ověří, zda jste pochopil/a
                    danou problematiku.
</p>
                <p><strong>Krok 1</strong></p>
                <img src="/images/uvod/text/register_button.jpg" className="img-fluid" alt="Registrační tlačítko" />
                <br />
                <p>
                    Na obrázku (pro lepší viditelnost v zeleném rámečku) vidíte tlačítko,
    to stačí stisknout a zobrazí se Vám <i>okno</i>, kde se nachází registrační formulář, který musíte vyplnit,
    abyste měli přístup k výukovým textům a testům.
</p>
                <br />
                <p><strong>Krok 2</strong></p>
                <img src="/images/uvod/text/Registration.jpg" className="img-fluid" alt="Registrační formulář" />
                <br />
                <p>
                    Pro lepší viditelnost jsou zde pro zvýraznění políček, které je nutné vyplnit, použity barevné rámečky. První červený rámeček obsahuje pole
                    Jméno a Příjmení, do prvního políčka vyplníte své jméno a do políčka pod ním své příjmení. Tato pole musí být vyplněna.
</p>
                <p>
                    Potom, co vyplníte svoje údaje, musíte vyplnit svoji přezdívku/uživatelské jméno. Může to být třeba Vaše křestní jméno nebo jméno i příjmení -
    Př. <i>Jiri, Jiri.Novak.</i> Fantazii se meze nekladou, je pouze nutné, abyste si svoji přezdívku pamatovali pro příští přihlášení.
</p>
                <p>
                    Poslední políčko, které je nutné vyplnit, je heslo. Heslo musí obsahovat minimálně 5 znaků. Může být samozřejmě i delší. Heslo může obsahovat
                    jak písmena tak číslice. Heslo musíte zadat dvakrát stejně. Slouží to jako kontrola kvůli možnému překlepu na klávesnici.
                    U hesla je také nutné, abyste si ho pamatovali, budete ho potřebovat pro další přihlášení na této stránce.
</p>
                <p>Všechna pole musí být vyplněná. Pokud máte hotovo, stiskněte modré tlačítko - <strong>Zaregistrovat se</strong></p>
                <p><strong>Může se Vám stát</strong></p>
                <img src="/images/uvod/text/registet-vyjimka1.jpg" className="img-fluid" alt="Vyjimka 1" />
                <br />
                <p>
                    Pokuď se Vám ukáže tato hláška (pro zvýraznění je na obrázku podtržená červeně), znamená to, že přezdívka, kterou jste si vybrali
                    už je zabraná. Proto si musíte vymyslet jinou přezdívku.
</p>
                <br />
                <img src="/images/uvod/text/register-vyjimka2.jpg" className="img-fluid" alt="Vyjimka 2" />
                <br />
                <p>
                    Pokud se Vám objeví tato hláška, která je opět pro zvýraznění na obrázku podtržená červeně, znamená to, že jste nesplnil/a požadavky, které jsou
                    potřeba pro platné heslo. Tyto požadavky jsou, že minimální délka hesla je 5 znaků (písmena i čísla). A obě hesla, která jste napsal/a
                    se musejí naprosto shodovat. Pokud se hesla neshodují, ukáže se hláška, která je na obrázku podtržená zeleně. Zkuste tedy heslo napsat znovu a opatrněji.
</p>
                <p>Pokud se Vám objeví jedna z těchto dvou hlášek nebo snad obě, vymyslete si novou přezdívku a heslo a pokračujte podle <strong>Kroku 2</strong></p>
                <p><strong>Nyní prosím klikněte na modré tlačítko - Jít se zaregistrovat</strong></p>
                <div className="form-group">
                    <div className="col-sm-2 col-sm-10">
                        <Link className="btn btn-primary pull-right" to="/Register">Jít se zaregistrovat</Link>
                        <Link className="btn btn-primary" to="/" onClick={()=>this.props.handler('h')}>Zpět na hlavní stránku</Link>
                    </div>
                </div>
            </>
        );
    }
}

export default RAdviceComponent;
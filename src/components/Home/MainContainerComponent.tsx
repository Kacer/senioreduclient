import React, { Component, Props } from "react";
import NavComponent from "./NavComponent";
import LAdviceComponent from "./LAdviceComponent";
import '../../css/blog.css';
import HomeComponent from "./HomeComponent";
import RAdviceComponent from "./RAdviceComponent";

interface Prop {
    date: string
    defCom: string
}

interface State {
   switchComp: string;
}

class MainContainerComponent extends React.Component<Prop, State> {
    constructor(props:Prop){
        super(props);
        this.state = {switchComp: this.props.defCom};
        document.title = "Výuka pro seniory";
        this.handleToUpdate  = this.handleToUpdate.bind(this);
    }

    renderBody(page:string) {
        if(page === "l"){
            return(
                <LAdviceComponent handler={this.handleToUpdate}/>
            )
        }else if(page === "r"){
            return(
                <RAdviceComponent handler={this.handleToUpdate}/>
              )
        }else{
            return(
                <HomeComponent/>
            )
        }
       
    }

    handleToUpdate(comp:any){
        this.setState({switchComp: comp});
    }
    
    render() {
        return (
            <>
            <div className="container">
                <NavComponent today={new Date().toLocaleDateString()}/>
                <br/>
                <div className="mb-3">
                    <img className="d-block w-100 img-fluid" src="/images/uvodniFotka.jpg" alt="úvodní fotka"/>
                </div>

                <div id="ajaxText"></div>
                <div className="row mb-2">
                    <div className="col-md-6">
                        <div className="card flex-md-row mb-4 box-shadow h-md-250">
                            <div className="card-body d-flex flex-column align-items-start">
                                <strong className="d-inline-block mb-2">Registrace</strong>
                                <h3 className="mb-0">
                                    <a className="text-dark" onClick={() => this.setState({switchComp: "r"})} href="javascript:void(0);">Jak se registrovat</a>
                                </h3>
                                <p className="card-text mb-auto">V tomto krátkém článku si pomocí obrázků ukážeme, jak se můžete zaregistrovat na této stránce, a tím získat přístup ke všem testům, které Vám pomohou s prací s počítačem a nternetem.</p>
                                <a onClick={() => this.setState({ switchComp: "r" })} href="javascript:void(0);">Pokračovat ve čtení</a>
                            </div>
                            <img className="card-img-right flex-auto d-none d-lg-block" src="/images/register.jpg" alt="Card image register"/>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="card flex-md-row mb-4 box-shadow h-md-250">
                            <div className="card-body d-flex flex-column align-items-start">
                                <strong className="d-inline-block mb-2">Přihlášení</strong>
                                <h3 className="mb-0">
                                <a className="text-dark" onClick={() => this.setState({switchComp: "l"})} href="javascript:void(0);">Jak se přihlásit</a>
                                </h3>
                                <p className="card-text mb-auto">V tomto článku si ukážeme, jak postupovat v případě, že již máme vytvořený účet a cheme se přihlásit.</p>
                                <a onClick={() => this.setState({ switchComp: "l" })} href="javascript:void(0);">Pokračovat ve čtení</a>
                            </div>
                            <img className="card-img-right flex-auto d-none d-lg-block" src="/images/login.jpg" alt="Card image login"/>
                        </div>
                    </div>
                </div>
            </div>

            <main role="main" className="container">
                <div className="row">
                    <div className="col-md-8">
                        <h3 className="pb-3 mb-4 font-italic border-bottom">
                            Zde si můžete přečíst potřebné informace
                        </h3>
                        <div id="switching">
                            {this.renderBody(this.state.switchComp)}
                        </div>
                    </div>
                    <aside className="col-md-4">
                        <div className="p-3 mb-3 bg-light rounded">
                            <h4 className="font-italic">O této stránce</h4>
                            <p className="card-text mb-auto">Tato stránka vznikla jako praktická část bakalářské práce na téma <i>Vývoj webu pro podporu vzdělávání seniorů</i> na Fakultě infomratiky a managementu UHK, katedře informačních technologií v letech 2018/2019</p>
                        </div>
                    </aside>
                </div>
            </main>

            <footer className="blog-footer">
                <div className="container">
                    <span className="text-muted">© Naboo inc. - {this.props.date} </span>
                </div>
            </footer>
            </>
        );
    }
}

export default MainContainerComponent;
import React from "react";
import { Link } from "react-router-dom";

interface Prop {
    today: string
}

interface State {
    // empty
}

class NavComponent extends React.Component<Prop, State> {
    render() {
        return (
            <nav className="navbar navbar-expand navbar-dark fixed-top bg-dark">
            <div className="collapse navbar-collapse">
                <div className="text-light">
                    {this.props.today}
                </div>
                <div className="mx-auto"></div>
                <ul className="navbar-nav pl-0">
                    <Link className="btn btn-outline-light my-2 my-sm-0 mr-1" to="/Register">Registrace</Link>
                </ul>
                <ul className="navbar-nav pl-0">
                    <Link className="btn btn-outline-light my-2 my-sm-0" to="/Login">Přihlásit se</Link>
                </ul>
            </div>
        </nav>
        );
    }
}

export default NavComponent;
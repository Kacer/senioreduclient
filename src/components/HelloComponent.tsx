import React from "react";

interface Prop {
    ownProp: string;
}

interface State {
    // empty
}

class HelloComponent extends React.Component<Prop, State> {
    render() {
        return (
            <nav className="navbar">
                <img src="/images/header.jpg" alt="úvodní fotka - zaregistrujte se nyní" />
                <p>SPA client - Výuka pro seniory</p>
                <p>Custom property: {this.props.ownProp}</p>
            </nav>
        );
    }
}

export default HelloComponent;
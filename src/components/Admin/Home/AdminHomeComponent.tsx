import React from "react";

interface Prop {

}

interface State {
    // empty
}

class AdminHomeComponent extends React.Component<Prop, State> {
    render() {
        return (
            <div className="container mx-auto dashboard-content">
                <h2>Vítejte, nyní jste přihlášen jako administrátor</h2>
                <p>Využíjte některou ze záložek na hlavní liště, pro správu dat v této aplikaci</p>
            </div>
        );
    }
}

export default AdminHomeComponent;



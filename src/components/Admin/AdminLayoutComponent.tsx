import React from "react";
import AdminHomeComponent from "./Home/AdminHomeComponent";
import { Link, Redirect } from "react-router-dom";
import AdminUsersComponent from "./Users/AdminUsersComponent";
import AdminTextComponent from "./Text/AdminTextComponent";
import AdminVocabularyComponent from "./Vocabulary/AdminVocabularyComponent";
import AdminMessageComponent from "./Message/AdminMessageComponent";
import AdminTestComponent from "./Test/AdminTestComponent";
import { ApiService } from "../../services/ApiService";
import { UserService } from "../../services/UserService";

interface Prop {

}

interface State {
    switchComp: string,
    loggedUser: any,
    loading: boolean
}

class AdminLayoutComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { switchComp: "home", loggedUser: null, loading: true };
        document.title = "Výukové prostředí";
        this.handleChildInfo = this.handleChildInfo.bind(this);
    }

    componentDidMount() {
        this.updateLoggedUser();
    }

    async updateLoggedUser() {
        ApiService.call("user/getLoggedUser", ApiService.GET)
            .then(response => {
                if(response.ok) {
                    response.json()
                        .then(user => {
                            this.setState({loggedUser: user, loading: false});
                        });
                } else {    // in all other cases we do not have the user
                    this.setState({loggedUser: null, loading: false});
                }
            }
        );
    }

    handleChildInfo(page: string) {
        this.setState({ switchComp: page });
    }

    renderBody(page: string) {
        let user = this.state.loggedUser;
        if(!this.state.loading && (user == null || user.Role.Identificator !== "admin")) {
            return <Redirect to="/User" />;
        }

        let id = -1;
        if (page.includes(":")) {
            id = parseInt(page.substr(page.indexOf(":") + 1));
            page = page.substr(0, page.indexOf(':'));
        }
        switch (page) {
            case "home":
                return (
                    <AdminHomeComponent />
                )
            case "uzivatele":
                return (
                    <AdminUsersComponent parentHandler={this.handleChildInfo} />
                )
            case "text":
                return (
                    <AdminTextComponent parentHandler={this.handleChildInfo} />
                )
            case "slovnik":
                return (
                    <AdminVocabularyComponent parentHandler={this.handleChildInfo} />
                )
            case "zpravy":
                return (
                    <AdminMessageComponent parentHandler={this.handleChildInfo} />
                )
            case "test":
                return (
                    <AdminTestComponent parentHandler={this.handleChildInfo} artId={id} />
                )
            default:
                return (
                    <></>
                )
        }
    }

    render() {
        return (
            <div className="dashboard-main-wrapper">
                <div className="dashboard-header">
                    <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
                        <a className="nav-link mb-1" href="/Admin"
                            onClick={e => { e.preventDefault(); this.setState({ switchComp: "home" }) }}>Hlavní stránka</a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse ml-3 mb-1" id="navbarCollapse">
                            <ul className="navbar-nav mr-auto">
                                <ul className="navbar-nav mr-auto">
                                    <li className="nav-item active">
                                        <a className="nav-link" href="/Admin"
                                            onClick={e => { e.preventDefault(); this.setState({ switchComp: "uzivatele" }) }}>
                                            Přehled uživatelů</a>
                                    </li>
                                    <li className="nav-item active">
                                        <a className="nav-link" href="/Admin"
                                            onClick={e => { e.preventDefault(); this.setState({ switchComp: "text" }) }}>Správa textů</a>
                                    </li>
                                    <li className="nav-item active">
                                        <a className="nav-link" href="/Admin"
                                            onClick={e => { e.preventDefault(); this.setState({ switchComp: "slovnik" }) }}>Správa slovníku</a>
                                    </li>
                                    <li className="nav-item active">
                                        <a className="nav-link" href="/Admin"
                                            onClick={e => { e.preventDefault(); this.setState({ switchComp: "zpravy" }) }}>Zprávy</a>
                                    </li>
                                </ul>
                            </ul>
                            <div className="form-inline mt-2 mt-md-0">
                                <Link className="btn btn-outline-primary my-2 my-sm-0 mr-1" to="/User">Uživatelský panel</Link>
                            </div>
                            <div className="form-inline mt-2 mt-md-0">
                                <a className="btn btn-outline-primary my-2 my-sm-0" href="/" onClick={
                                    (e) => {
                                        UserService.logOut();
                                    }}>Odhlášení</a>
                            </div>
                        </div>
                    </nav>
                </div>
                <div id="dynamicContent">
                    {this.renderBody(this.state.switchComp)}
                </div>
            </div>
        );
    }
}

export default AdminLayoutComponent;
import React from "react";
import { ApiService } from "../../../services/ApiService";

interface Prop {
    handler: any;
}

interface State {
    listWord: Array<any>,
    loading: boolean
}

class AdminVocabularyTableComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { loading: true, listWord: [] };
    }

    componentDidMount() {
        this.loadWords();
    }

    async loadWords() {
        let path = "vocabulary/getAll";
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let words = await response.json();
            this.setState({ listWord: words, loading: false });
        } if (response.status === 401) {
            this.setState({ listWord: [], loading: false });
        }
    }

    renderItems() {
        const items: any = [];
        if (!this.state.loading) {
            this.state.listWord.forEach((word: any) => {
                items.push(
                    <tr key={word.Id}>
                        <td><strong>{word.Word}</strong></td>
                        <td>{word.Description}</td>
                        <td><a href="/Admin" onClick={e => {
                            e.preventDefault();
                            this.props.handler("updateWord:"+word.Id);
                        }}>Upravit</a></td>
                        <td><a href="/Admin"
                            onClick={e => { e.preventDefault(); this.delWord(word.Id); }}>Odstranit</a></td>
                    </tr>
                )
            });
        }
        return (<>
            <thead className="thead-dark">
                <tr>
                    <th scope="col">Slovíčko</th>
                    <th scope="col">Význam</th>
                    <th scope="col">Akce</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                {items}
            </tbody>
        </>

        )

    }

    async delWord(id: any) {
        let path = "vocabulary/delete/" + id;
        const response = await ApiService.call(path, ApiService.DELETE);
        if (response.status === 200) {
            this.setState({ loading: true, listWord: [] });
            this.loadWords();
        }
    }

    render() {
        return (
            <div className="container-fluid dashboard-content">
                <table className="table">
                    {this.renderItems()}
                </table>
            </div>

        );
    }
}

export default AdminVocabularyTableComponent;
import React from "react";
import HelloComponent from "../../HelloComponent";
import AdminVocabularyTableComponent from "./AdminVocabularyTableComponent";
import AdminVocabularyCreateComponent from "./AdminVocabularyCreateComponent";
import AdminVocabularyEditComponent from "./AdminVocabularyEditComponent";

interface Prop {
    parentHandler: any;
}

interface State {
    content: string;
}

class AdminVocabularyComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { content: "home"};
        this.handleChildInfo = this.handleChildInfo.bind(this);
    }

    componentWillReceiveProps(){
        if (!this.state.content.includes("home")){
            this.setState({content: "home"})
        }
    }

    renderBody(page: string) {
        let id = -1;
        if(page.includes(":")){
            id = parseInt(page.substr(page.indexOf(":")+1));
            page =  page.substr(0, page.indexOf(':')); 
        }
        switch (page) {
            case "home":
                return (
                    <AdminVocabularyTableComponent handler={this.handleChildInfo} />
                )
            case "createWord":
                return (
                    <AdminVocabularyCreateComponent handler={this.handleChildInfo} />
                )
            case "updateWord":
                return (
                    <AdminVocabularyEditComponent id={id} handler={this.handleChildInfo} />
                )
            default:
                return (
                    <></>
                )
        }
    }

    handleChildInfo(info: string, top: boolean) {
        if (top) {
            this.props.parentHandler(info)
        } else {
            this.setState({ content: info });
        }
    }

    render() {
        return (
            <>
                <div className="nav-left-sidebar sidebar-dark">
                    <nav className="navbar navbar-expand-md navbar-light w-100">
                        <ul className="navbar-nav flex-column">
                            <li className="nav-divider">
                                <h5>Slovníček pojmů</h5>
                            </li>
                            <li className="nav-item">

                                <ul className="nav flex-column">
                                    <a className="btn btn-primary" href="javascript:void(0);"
                                        onClick={e => { e.preventDefault(); this.setState({ content: "createWord" }) }}>Přidat slovíčko</a>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div className="dashboard-wrapper">
                    {this.renderBody(this.state.content)}
                </div>
            </>
        );
    }
}

export default AdminVocabularyComponent;
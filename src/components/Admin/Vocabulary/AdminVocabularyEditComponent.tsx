import React from "react";
import { FormService } from "../../../services/FormService";
import { ApiService } from "../../../services/ApiService";
import { isNull } from "util";

interface Prop {
    handler: any;
    id: number
}

interface State {
    inputWord: any,
    loading: boolean,
    word: string,
    description: string,
    errors: {
        word: string,
        description: string
    };
}

class AdminVocabularyEditComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            inputWord: null,
            word: '',
            loading: true,
            description: '',
            errors: {
                word: '',
                description: ''
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.loadWord(this.props.id);
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({ loading: true, inputWord: null });
        this.loadWord(nextProps.id);
    }

    async loadWord(id: number) {
        let path = "vocabulary/getById/" + id;
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let w = await response.json();
            this.setState({ inputWord: w, loading: false, word: w.Word, description: w.Description });
        } if (response.status === 400 || response.status === 404) {
            this.setState({ inputWord: null, loading: false });
        }
    }

    handleSubmit(event: any) {
        if (this.validateForm(this.state.errors, this.state)) {
            this.sendData();
        }
        event.preventDefault();
    }

    handleChange(event: any) {
        const newState = FormService.getStateFromEvent(event);
        this.setState(newState);
        this.validateState(event.target.name, event.target.value);
    }

    validateForm(errors: any, states: any) {
        let valid = true;
        Object.keys(errors).forEach(
            (key: string) => {
                this.validateState(key, states[key]);
            }
        );
        Object.values(errors).forEach(
            (val: any) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    validateState(name: any, value: any) {
        let errorsArray = this.state.errors;
        switch (name) {
            case "word":
                errorsArray.word = value.length < 1 ? 'Název slovíčka je vyžadován.' : '';
                break;
            case "description":
                errorsArray.description = value.length < 1 ? 'Popis slovíčka je vyžadován.' : '';
                break;
            default:
                break;
        }
        this.setState({ errors: errorsArray });
    }

    async sendData(): Promise<boolean> {
        var jsonData = {
            id: this.state.inputWord.Id,
            word: this.state.word,
            description: this.state.description,
        };
        const response = await ApiService.callWithBody("vocabulary/update", ApiService.PUT, jsonData);
        if (response.status === 400) {
            console.log("Something is wrong")
        } else if (response.status === 200) {
            this.props.handler("home");
        }
        return response.ok;
    }

    render() {
        if (!this.state.loading && !isNull(this.state.inputWord)) {
        return (
            <div className="container-fluid dashboard-content">
                <h2>Upravit slovíčko - {this.state.inputWord.Word}</h2>

                <form onSubmit={this.handleSubmit}><div className="container">
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Upravit slovíčko</label>
                        <div className="col-sm-10">
                        <input className="form-control" type="text" name="word" value={this.state.word} onChange={this.handleChange} />
                            <span className="field-validation-valid">{this.state.errors.word}</span>
                        </div>
                    </div>

                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Upravit vysvětlivku</label>
                        <div className="col-sm-10">
                        <textarea className="form-control" cols={20} name="description" rows={2} value={this.state.description} onChange={this.handleChange}>
                            {this.state.description}
                        </textarea>
                            <span className="field-validation-valid">{this.state.errors.description}</span>
                        </div>
                    </div>

                    <div className="form-group">
                        <div className="col-sm-2 col-sm-10">
                            <button type="submit" className="btn btn-primary">Upravit slovíčko</button>
                            <a href="/Admin" className="btn btn-primary pull-right"
                                onClick={e => {
                                    e.preventDefault(); 
                                    this.props.handler("home")
                                    }}>Zpět</a>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        );
        } else{
            return (<></>);
        }
    }
}

export default AdminVocabularyEditComponent;
import React from "react";
import TestAnswerComponent from "./TestAnswerComponent";

interface Prop {
    question: any;
    questionNumber: number;
    delHandler: any;
    onQuestionChange: any;
}

interface State {
    mapOfAnswers: Map<number, any>;
}

class TestQuestionComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            mapOfAnswers: new Map
        }

        this.props.question.Answers.forEach((answer: any) => {
            this.addAnswer(answer);
        });

        this.handleDeleteAnswer = this.handleDeleteAnswer.bind(this);
        this.handleAnswerChange = this.handleAnswerChange.bind(this);
        this.handleQuestionChange = this.handleQuestionChange.bind(this);
        this.handleQuestionChangeValue = this.handleQuestionChangeValue.bind(this);
    }

    addAnswer(answer: any) {
        let answerMap: Map<number, any> = this.state.mapOfAnswers;
        answerMap.set(answerMap.size + 1, answer);
        this.handleQuestionChange(null, null);
    }

    handleDeleteAnswer(answerNumber: number) {
        let oldAnswerMap: Map<number, any> = this.state.mapOfAnswers;
        let newAnswerMap: Map<number, any> = new Map;
        let answerCounter = 1;
        
        oldAnswerMap.delete(answerNumber);

        // remap number for each answer from the start
        oldAnswerMap.forEach((element) => {
            newAnswerMap.set(answerCounter++, element);
        });

        this.setState({mapOfAnswers: newAnswerMap});
        this.handleQuestionChange(null, null);
    }

    handleAnswerChange(answer: any) {
        this.handleQuestionChange(null, answer);
    }

    handleQuestionChange(e: any, answer: any) {
        // get array of answers
        let answers: any = [];
        this.state.mapOfAnswers.forEach((element) => {
            answers.push(element);
        });

        // copy question object
        let question: any = {
            Question: this.props.question.Question,
            Hint: this.props.question.Hint,
            Answers: answers
        };
        if(this.props.question.hasOwnProperty('Id')) {
            question.Id = this.props.question.Id;
        } 
        question.number = this.props.questionNumber;
        

        if(answer != null && answer != undefined && answer != '') {
            let answerToUpdate: any = this.state.mapOfAnswers.get(answer.number);
            if(answerToUpdate != undefined) {
                delete answer.number;
                Object.assign(answerToUpdate, answer);
            }
        }
        
        if(e != null) {
            switch(e.target.name) {
                case 'question':
                    question.Question = e.target.value;
                    break;
                case 'hint':
                    question.Hint = e.target.value;
                    break;
            }
        }

        this.props.onQuestionChange(question);
    }

    handleQuestionChangeValue(e: any) {
        this.handleQuestionChange(e, null);
    }

    render() {
        let listOfAnswerComponents: any = [];
        const question = this.props.question;
        let answerNumber = 1;
        question.Answers.forEach((answer: any) => {
            listOfAnswerComponents.push(
                <TestAnswerComponent answer={answer} answerNumber={answerNumber++} delHandler={this.handleDeleteAnswer} onAnswerChange={this.handleAnswerChange}/>
            );
        });
        
        return (
            <div className="card-body ">
                <h5>Otázka: </h5>
                <div className="input-group mb-2">
                    <div className="input-group-prepend">
                        <span className="input-group-text">{this.props.questionNumber}. otázka: </span>
                    </div>
                    <input className="form-control" name="question" type="text" value={this.props.question.Question} onChange={this.handleQuestionChangeValue} />
                    <div className="input-group-append">
                        <a className="btn btn-danger" href="/Admin" onClick={(e) => {
                            e.preventDefault();
                            this.props.delHandler(this.props.questionNumber);
                        }}>Odstranit</a>
                    </div>
                </div>
                <div className="input-group mb-4">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Popis: </span>
                    </div>
                    <input className="form-control" name="hint" placeholder="Popis není povinný..." type="text" value={this.props.question.Hint} onChange={this.handleQuestionChangeValue}  />
                </div>
                <h5>Odpovědi: </h5>
                {listOfAnswerComponents}

                <a className="btn btn-success" href="/Admin" onClick={(e) => {
                            e.preventDefault();
                            this.addAnswer({
                                Answer: '',
                                IsCorrect: false
                            });
                        }}>Přidat odpoveď</a>
            </div>
        );
    }
}

export default TestQuestionComponent;
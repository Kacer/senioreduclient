import React from "react";
import { FormService } from "../../../services/FormService";
import { ApiService } from "../../../services/ApiService";
import TestQuestionComponent from "./TestQuestionComponent";

interface Prop {
    handler: any;
    article: any;
}

interface State {
    test: any,
    loading: boolean, 
    mapOfQuestions: Map<number, any>,
}

class AdminTestCreateComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            test: {},
            loading: true,
            mapOfQuestions: new Map, 
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDeleteQuestion = this.handleDeleteQuestion.bind(this);
        this.handleQuestionChange = this.handleQuestionChange.bind(this);
        this.handleCheckChange = this.handleCheckChange.bind(this);
    }

    componentDidMount() {
        this.fetchTest(this.props.article.TestId);
    }

    fetchTest(id: number): any {
        ApiService.call('test/getById/' + id, ApiService.GET)
            .then(response => {
                if(response.ok) {
                    response.json()
                        .then(test => {
                            let questionNumber: number = 1;
                            let mapOfQuestions: Map<number, any> = new Map();
                            test.Questions.forEach((question: any) => {
                                mapOfQuestions.set(questionNumber++, question);
                            });
                            this.setState({test: test, mapOfQuestions: mapOfQuestions, loading: false});
                        }
                    );
                } else {
                    let test = {
                        Published: false,
                        Questions: [
                            {
                                Question: '',
                                Hint: '',
                                Answers: [
                                    {
                                        Answer: '',
                                        IsCorrect: false
                                    }
                                ]
                            }
                        ]
                    };
                    this.setState({test: test, loading: false});
                }
            }
        );
    }

    handleSubmit(event: any) {
        event.preventDefault();
        if(this.validateTest()) {
            this.sendData();
        } 
    }

    handleChange(event: any, question: any) {
        let test = this.state.test;

        if(event != null) {
            switch(event.target.name) {
                case 'published':
                    test.Published = event.target.checked;
                    break;
            }
        }
        
        if(question != null && question != undefined && question != '') {
            let questionToUpdate = this.state.mapOfQuestions.get(question.number);
            if(questionToUpdate != undefined) {
                delete question.number;
                Object.assign(questionToUpdate, question);
            }
        }

        let questions: any = [];
        this.state.mapOfQuestions.forEach((element) => {
            questions.push(element);
        });
        test.Questions = questions;

        this.setState({test: test});
    }

    handleCheckChange(event: any) {
        this.handleChange(event, null);
    }

    handleQuestionChange(question: any) {
        this.handleChange(null, question);
    }

    validateTest(): boolean {
        let test: any = this.state.test;
        if(test.Questions.length === 0) {
            alert('Test musí obsahovat alespoň jednu otázku');
            return false;
        }

        for(let q: number = 0; q < test.Questions.length; q++) {
            let question: any = test.Questions[q];
            
            if(question.Question == '') {
                alert('V testu je prázdná otázka');
                return false;
            }

            if(question.Answers.length  < 2) {
                alert('Každá otázka musí obsahovat alespoň 2 odpovědi, přičemž musí být vždy alespoň jedna odpověď správná');
                return false;
            }

            let correctAnswerCount = 0;
            for(let a: number = 0; a < question.Answers.length; a++) {
                let answer: any = question.Answers[a];
                
                if(answer.IsCorrect) {
                    correctAnswerCount++;
                }
                if(answer.Answer == '') {
                    alert('V testu je prázdná odpověď');
                    return false;
                }
            }

            if(correctAnswerCount === 0) {
                alert('V testuje je otázka, která nemá označenou žádnou správnou odpověď');
                return false;
            }
        }

        return true;
    }

    sendData() {
        if(this.state.test.Id !== undefined) {
            // update test
            ApiService.callWithBody("test/update", ApiService.PUT, this.state.test)
                .then(response => {
                    if(response.ok) {
                        // redirect to all articles
                        this.props.handler("text", true);
                    } else {
                        alert("Test se nepodařilo uložit");
                    }
                });

            return;
        }

        // create test
        ApiService.callWithBody("test/create", ApiService.POST, this.state.test)
            .then(response => {
                if(response.ok) {
                    response.json()
                        .then(test => {
                            // update article
                            let article: any = JSON.parse(JSON.stringify(this.props.article)); // deep copy of article
                            article.TestId = test.Id;
                            ApiService.callWithBody("article/update", ApiService.PUT, article)
                                .then(res => {
                                    if(res.ok) {
                                        // redirect to all articles
                                        this.props.handler("text", true);
                                    } else {
                                        // test was created, but article was not updated...
                                        alert("Test byl vytvořen, ale nepodařilo se updatnout článek, ke kterému patří => test nebude vidět");
                                    }
                                }
                            );
                        }
                    );
                } else {
                    // test was not created...
                    alert("Test se nepodařilo uložit");
                }             
            }
        );
    }

    addQuestion(question: any) {
        let questionMap: Map<number, any> = this.state.mapOfQuestions;
        questionMap.set(questionMap.size + 1, question);
        this.handleChange(null, null);
    }

    handleDeleteQuestion(questionNumber: number) {
        let oldQuestionMap: Map<number, any> = this.state.mapOfQuestions;
        let newQuestionMap: Map<number, any> = new Map;
        let questionCounter = 1;

        oldQuestionMap.delete(questionNumber);

        oldQuestionMap.forEach((element) => {
            newQuestionMap.set(questionCounter++, element);
        });

        this.setState({mapOfQuestions: newQuestionMap});
        this.handleChange(null, null);
    }

    render() {
        if(this.state.loading) {
            return <></>
        }

        let listOfQuestionComponents: any = [];
        const test = this.state.test;
        let questionNumber = 1;
        test.Questions.forEach((question: any) => {
            listOfQuestionComponents.push(
                <TestQuestionComponent question={question} questionNumber={questionNumber++} delHandler={this.handleDeleteQuestion} onQuestionChange={this.handleQuestionChange} />
            );
        });

        return (
            <div className="container-fluid dashboard-content">
                <div className="row">
                    <form className="col-12" onSubmit={this.handleSubmit}>
                        <div className="card">
                            <div className="card-header">
                                {this.props.article.Name}
                            </div>
                            {listOfQuestionComponents}
                        </div>

                        <a className="btn btn-success" href="/Admin" onClick={(e) => {
                            e.preventDefault();
                            this.addQuestion({
                                Question: '',
                                Hint: '',
                                Answers: [
                                    {
                                        Answer: '',
                                        IsCorrect: false
                                    }
                                ]
                            });
                        }}>Přidat testovou otázku</a>
                        <br/>
                        
                        <div className="custom-control custom-checkbox">
                            <input className="custom-control-input" id="defaultChecked2" name="published" type="checkbox" checked={this.state.test.Published} onChange={this.handleCheckChange} />
                            <label className="custom-control-label" htmlFor="defaultChecked2">Publikovat</label>
                        </div>

                        <button type="submit" className="btn btn-primary">Uložit Test</button>
                    </form>
                </div >
            </div >
        );
    }
}

export default AdminTestCreateComponent;
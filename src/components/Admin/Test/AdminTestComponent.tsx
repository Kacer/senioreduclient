import React from "react";
import { ApiService } from "../../../services/ApiService";
import AdminTestCreateComponent from "./AdminTestCreateComponent";
interface Prop {
    parentHandler: any;
    artId: number;
}

interface State {
    content: string;
    loading: boolean;
    article: any;
}

class AdminTestComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = {
            content: "home",
            loading: true,
            article: null
        };
        this.handleChildInfo = this.handleChildInfo.bind(this);
    }

    componentDidMount() {
        this.loadArticle(this.props.artId);
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({ loading: true, article: null });
        this.loadArticle(nextProps.artId);
    }

    async loadArticle(id: number) {
        let path = "article/getById/" + id;
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let art = await response.json();
            this.setState({ article: art, loading: false });
        } else {
            this.setState({ article: null, loading: false });
        }
    }

    renderBody(id: number) {
        if(id < 0) {
            return <></>;
        } else {
            return <AdminTestCreateComponent handler={this.handleChildInfo} article={this.state.article} />
        }
    }

    handleChildInfo(info: string, top: boolean) {
        if (top) {
            this.props.parentHandler(info)
        } else {
            this.setState({ content: info });
        }
    }


    render() {
        if (!this.state.loading && this.state.article) {
            return (
                <>
                    <div className="nav-left-sidebar sidebar-dark">
                        <nav className="navbar navbar-expand-md navbar-light w-100">
                            <ul className="navbar-nav flex-column">
                                <li className="nav-divider">
                                    <h5>Test</h5>
                                </li>
                                <li className="nav-item mb-2">
                                    <p>Zdravím!</p>
                                    <p>Vítejte na tvorbě testů.</p>
                                    <p>
                                        Zde můžete přidávat a odstraňovat otázky, otázka se vždy přidá na konec stránky. Když vytváříte test, pro uživatele
                                        není viditelný, dokud ho nepublikujete.
                                    </p>
                                    <ul className="nav flex-column">
                                        <a className="btn btn-primary" href="/Admin" onClick={(e) => {
                                            e.preventDefault();
                                            this.props.parentHandler("text");
                                        }}>Zpět na články</a>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                    </div>
                    <div className="dashboard-wrapper">
                        {this.renderBody(this.state.article.TestId)}
                    </div>
                </>
            );
        } else {
            return <></>;
        }
    }
}

export default AdminTestComponent;
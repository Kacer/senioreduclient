import React from "react";

interface Prop {
    answer: any;
    answerNumber: number;
    delHandler: any;
    onAnswerChange: any;
}

interface State {
    // empty
}

class TestAnswerComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);

        this.handleAnswerChange = this.handleAnswerChange.bind(this);
    }

    handleAnswerChange(e: any) {    
        let answer: any = JSON.parse(JSON.stringify(this.props.answer));    // deep copy of object
        switch(e.target.name) {
            case 'answer':
                answer.Answer = e.target.value;
                break;
            case 'correct':
                answer.IsCorrect = e.target.checked;
                break;
        }

        answer.number = this.props.answerNumber;
        
        this.props.onAnswerChange(answer);
    }

    render() {
        return (
            <div className="input-group mb-2">
                <div className="input-group-prepend">
                    <span className="input-group-text">{this.props.answerNumber}.</span>
                </div>

                <input className="form-control" name="answer" type="text" value={this.props.answer.Answer} onChange={this.handleAnswerChange} />
                <div className="input-group-append">
                    <div className="input-group-text green">
                        <span className="mr-2">Správně:</span>
                        <input className="143" name="correct" type="checkbox" checked={this.props.answer.IsCorrect} onChange={this.handleAnswerChange} />
                    </div>
                    <a className="btn btn-danger" href="/Admin" onClick={(e) => {
                            e.preventDefault();
                            this.props.delHandler(this.props.answerNumber);
                        }}>Odstranit</a>
                </div>
            </div>
        );
    }
}

export default TestAnswerComponent;
import React from "react";
import { ApiService } from "../../../services/ApiService";

interface Prop {
    handler: any;
}

interface State {
    listMessage: Array<any>,
    loading: boolean
}

class AdminMessageTableComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { loading: true, listMessage: [] };
    }

    componentDidMount() {
        this.loadMessages();
    }

    componentWillReceiveProps(){
        this.loadMessages();
    }

    async loadMessages() {
        let path = "message/getAll";
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let messages = await response.json();
            this.setState({ listMessage: messages, loading: false });
        } if (response.status === 401) {
            this.setState({ listMessage: [], loading: false });
        }
    }

    renderItems() {
        const items: any = [];
        if (!this.state.loading) {
            this.state.listMessage.forEach((mes: any) => {
                items.push(
                    <tr key={mes.Id}>
                        <td><a href="/Admin"
                            onClick={e => { e.preventDefault(); this.props.handler("detail:" + mes.Id) }}>Zobrazit</a></td>
                        <td>{mes.UserName}</td>
                        <td>{mes.Subject}</td>
                        <td> <a href="/Admin"
                            onClick={e => { e.preventDefault(); this.deleteMessage(mes.Id) }}>Odstranit</a></td>
                    </tr>
                )
            });
        }
        return (<>
            <thead className="thead-dark">
                <tr>
                    <th scope="col">Zpráva</th>
                    <th scope="col">Odesílatel</th>
                    <th scope="col">Předmět</th>
                    <th scope="col">Smazat zprávu</th>
                </tr>
            </thead>
            <tbody>
                {items}
            </tbody>
        </>

        )

    }
    async deleteMessage(id: any) {
        let path = "message/delete/" + id;
        const response = await ApiService.call(path, ApiService.DELETE);
        if (response.status === 200) {
            this.setState({ loading: true, listMessage: [] });
            this.loadMessages();
        }
    }

    render() {
        return (
            <div className="container mx-auto dashboard-content">
                <h2>Zprávy k vyřízení</h2>
                <table className="table">
                    {this.renderItems()}
                </table>
            </div>
        );
    }
}

export default AdminMessageTableComponent;
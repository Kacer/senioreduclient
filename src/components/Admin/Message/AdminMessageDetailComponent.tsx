import React from "react";
import { ApiService } from "../../../services/ApiService";
import { isNull } from "util";

interface Prop {
    handler: any;
    id: number;
}

interface State {
    message: any,
    loading: boolean
}

class AdminMessageDetailComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { loading: true, message: null };
    }

    componentDidMount() {
        this.loadUser(this.props.id);
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({ loading: true, message: null });
        this.loadUser(nextProps.type);
    }

    async loadUser(id: number) {
        let path = "message/getById/" + id;
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let mes = await response.json();
            this.setState({ message: mes, loading: false });
        } if (response.status === 400 || response.status === 404) {
            this.setState({ message: null, loading: false });
        }
    }

    async removeMessage(id: number) {
        let path = "message/delete/" + id;
        const response = await ApiService.call(path, ApiService.DELETE);
        if (response.status === 200) {
            this.props.handler("table");
        }
    }

    render() {
        if (!this.state.loading && !isNull(this.state.message)) {
            return (
                <>
                    <div className="container mx-auto dashboard-content">
                        <div className="card mx-auto">
                            <div className="card-header">
                                <h4 className="text-center">Zpráva od uživatele {this.state.message.UserName}</h4>
                            </div>
                            <div className="p-4">
                                <div className="mb-2">
                                    <label>E-mail</label><br />
                                    <a href="mailto:nela.ctvrteckova@gmail.com">{this.state.message.Sender}</a>
                                </div>
                                <div className="mb-2">
                                    <label>Předmět</label><br />
                                    {this.state.message.Subject}
                                </div>
                                <div className="mb-2">
                                    <label>Zpráva</label><div></div>
                                    {this.state.message.Text}
                                </div>
                            </div>
                        </div>
                        <a className="btn btn-outline-primary mb-3" href="/Admin" onClick={e => {
                            e.preventDefault();
                            this.props.handler("table");
                        }}>Zpět na všechny zprávy</a>
                        <a className="btn btn-outline-primary mb-3" href="/Admin" onClick={e => {
                            e.preventDefault();
                            this.removeMessage(this.state.message.Id);
                        }}>Odstranit zprávu</a>
                    </div>
                </>
            );
        }
        return (
            <></>
        )
    }

}

export default AdminMessageDetailComponent;
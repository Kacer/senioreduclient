import React from "react";
import AdminMessageTableComponent from "./AdminMessageTableComponent";
import AdminMessageDetailComponent from "./AdminMessageDetailComponent";

interface Prop {
    parentHandler: any;
}

interface State {
    content: string;
}

class AdminMessageComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { content: "table" };
        this.handleChildInfo = this.handleChildInfo.bind(this);
    }

    componentWillReceiveProps(){
        if (this.state.content.includes("detail")){
            this.setState({content: "table"})
        }
    }

    renderBody(page: string) {
        let id = -1;
        if(page.includes(":")){
            id = parseInt(page.substr(page.indexOf(":")+1));
            page =  page.substr(0, page.indexOf(':')); 
        }
        switch (page) {
            case "detail":
                return (
                    <AdminMessageDetailComponent id={id} handler={this.handleChildInfo}/>
                )
            case "table":
                return (
                    <AdminMessageTableComponent handler={this.handleChildInfo} />
                )
            default:
                return (
                    <></>
                )
        }
    }

    handleChildInfo(info: string, top: boolean) {
        if (top) {
            this.props.parentHandler(info)
        } else {
            this.setState({ content: info });
        }
    }

    render() {
        return (
            <>
                {this.renderBody(this.state.content)}
            </>
        );
    }
}

export default AdminMessageComponent;
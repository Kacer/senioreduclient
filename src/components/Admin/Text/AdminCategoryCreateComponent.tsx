import React from "react";
import { FormService } from "../../../services/FormService";
import { ApiService } from "../../../services/ApiService";

interface Prop {
    handler: any;
}

interface State {
    name: string,
    description: string,
    errors: {
        name: string,
        description: string
    };
}

class AdminCategoryCreateComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            name: '',
            description: '',
            errors: {
                name: '',
                description: ''
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event: any) {
        if (this.validateForm(this.state.errors, this.state)) {
            this.sendData();
        }
        event.preventDefault();
    }

    handleChange(event: any) {
        const newState = FormService.getStateFromEvent(event);
        this.setState(newState);
        this.validateState(event.target.name, event.target.value);
    }

    validateForm(errors: any, states: any) {
        let valid = true;
        Object.keys(errors).forEach(
            (key: string) => {
                this.validateState(key, states[key]);
            }
        );
        Object.values(errors).forEach(
            (val: any) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    validateState(name: any, value: any) {
        let errorsArray = this.state.errors;
        switch (name) {
            case "name":
                errorsArray.name = value.length < 1 ? 'Název kategorie je vyžadován.' : '';
                break;
            case "description":
                errorsArray.description = value.length < 1 ? 'Popis kategorie je vyžadován.' : '';
                break;
            default:
                break;
        }
        this.setState({ errors: errorsArray });
    }

    async sendData(): Promise<boolean> {
        var jsonData = {
            name: this.state.name,
            description: this.state.description,
        };
        const response = await ApiService.callWithBody("category/create", ApiService.POST, jsonData);
        if (response.status === 400) {
            console.log("Something is wrong")
        } else if (response.status === 200) {
            this.props.handler("text", true);
        }
        return response.ok;
    }

    render() {
        return (
            <div className="container-fluid dashboard-content">
                <h2>Vytvoření nové kategorie</h2>
                <form onSubmit={this.handleSubmit}>
                    <div className="container">
                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label" id="inputSuccess1">Název kategorie</label>
                            <div className="col-sm-10">
                                <input className="form-control" name="name" type="text" value={this.state.name} onChange={this.handleChange} />
                                <span className="field-validation-valid">{this.state.errors.name}</span>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Popis kategorie</label>
                            <div className="col-sm-10">
                                <textarea className="form-control" cols={20} name="description" rows={2}
                                    value={this.state.description} onChange={this.handleChange}></textarea>
                                <span className="field-validation-valid">{this.state.errors.description}</span>
                            </div>
                        </div>

                        <div className="form-group">
                            <div className="col-sm-2 col-sm-10">
                                <button type="submit" className="btn btn-primary">Přidat kategorii</button>
                                <a href="/Admin" className="btn btn-primary pull-right" onClick={e => {
                                    e.preventDefault();
                                    this.props.handler("home");
                                }}>Zpět</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default AdminCategoryCreateComponent;
import React from "react";
import { FormService } from "../../../services/FormService";
import { ApiService } from "../../../services/ApiService";
import { isNull } from "util";

interface Prop {
    handler: any;
    catId: number
}

interface State {
    inputCategory: any,
    loading: boolean,
    name: string,
    description: string,
    errors: {
        name: string,
        description: string
    };
}

class AdminCategoryEditComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            inputCategory: null,
            name: '',
            loading: true,
            description: '',
            errors: {
                name: '',
                description: ''
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.loadCategory(this.props.catId);
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({ loading: true, inputCategory: null });
        this.loadCategory(nextProps.catId);
    }

    async loadCategory(id: number) {
        let path = "category/getById/" + id;
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let cat = await response.json();
            this.setState({ inputCategory: cat, loading: false, name: cat.Name, description: cat.Description });
        } if (response.status === 400 || response.status === 404) {
            this.setState({ inputCategory: null, loading: false });
        }
    }

    handleSubmit(event: any) {
        if (this.validateForm(this.state.errors, this.state)) {
            this.sendData();
        }
        event.preventDefault();
    }

    handleChange(event: any) {
        const newState = FormService.getStateFromEvent(event);
        this.setState(newState);
        this.validateState(event.target.name, event.target.value);
    }

    validateForm(errors: any, states: any) {
        let valid = true;
        Object.keys(errors).forEach(
            (key: string) => {
                this.validateState(key, states[key]);
            }
        );
        Object.values(errors).forEach(
            (val: any) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    validateState(name: any, value: any) {
        let errorsArray = this.state.errors;
        switch (name) {
            case "name":
                errorsArray.name = value.length < 1 ? 'Název slovíčka je vyžadován.' : '';
                break;
            case "description":
                errorsArray.description = value.length < 1 ? 'Popis slovíčka je vyžadován.' : '';
                break;
            default:
                break;
        }
        this.setState({ errors: errorsArray });
    }

    async sendData(): Promise<boolean> {
        var jsonData = {
            id: this.state.inputCategory.Id,
            name: this.state.name,
            description: this.state.description,
        };
        const response = await ApiService.callWithBody("category/update", ApiService.PUT, jsonData);
        if (response.status === 400) {
            console.log("Something is wrong")
        } else if (response.status === 200) {
            this.props.handler("text", true);
        }
        return response.ok;
    }

    render() {
        if (!this.state.loading && !isNull(this.state.inputCategory)) {
        return (
            <div className="container-fluid dashboard-content">
                <h2>Upravit kategorii - {this.state.inputCategory.Name}</h2>
                <form onSubmit={this.handleSubmit}>
                    <div className="container">
                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label" id="inputSuccess1">Název kategorie</label>
                            <div className="col-sm-10">
                                <input className="form-control" name="name" type="text" value={this.state.name} onChange={this.handleChange} />
                                <span className="field-validation-valid">{this.state.errors.name}</span>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Popis kategorie</label>
                            <div className="col-sm-10">
                                <textarea className="form-control" cols={20} name="description" rows={2}
                                    value={this.state.description} onChange={this.handleChange}></textarea>
                                <span className="field-validation-valid">{this.state.errors.description}</span>
                            </div>
                        </div>

                        <div className="form-group">
                            <div className="col-sm-2 col-sm-10">
                                <button type="submit" className="btn btn-primary">Upravit kategorii</button>
                                <a href="/Admin" className="btn btn-primary pull-right" onClick={e => {
                                    e.preventDefault();
                                    this.props.handler("home");
                                }}>Zpět</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
        } else{
            return (<></>);
        }
    }
}

export default AdminCategoryEditComponent;
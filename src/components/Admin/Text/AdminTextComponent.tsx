import React from "react";
import HelloComponent from "../../HelloComponent";
import AdminTextTableComponent from "./AdminTextTableComponent";
import { ApiService } from "../../../services/ApiService";
import AdminArticleDetailComponent from "./AdminArticleDetailComponent";
import AdminCategoryCreateComponent from "./AdminCategoryCreateComponent";
import AdminCategoryEditComponent from "./AdminCategoryEditComponent";
import AdminArticleCreateComponent from "./AdminArticleCreateComponent";
import AdminArticleEditComponent from "./AdminArticleEditComponent";

interface Prop {
    parentHandler: any;
}

interface State {
    content: string;
    loading: boolean;
    listCat: Array<any>;
}

class AdminTextComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = {
            content: "home",
            loading: true,
            listCat: []
        };
        this.handleChildInfo = this.handleChildInfo.bind(this);
    }

    componentDidMount() {
        this.loadCategories()
    }

    componentWillReceiveProps() {
        this.loadCategories();
        this.setState({ content: "home" });
    }

    async loadCategories() {
        let path = "category/getAll";
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let cats = await response.json();
            this.setState({ listCat: cats, loading: false });
        } if (response.status === 401) {
            this.setState({ listCat: [], loading: false });
        }
    }


    renderBody(page: string) {
        let id = -1;
        if (page.includes(":")) {
            id = parseInt(page.substr(page.indexOf(":") + 1));
            page = page.substr(0, page.indexOf(':'));
        }

        switch (page) {
            case "home":
                return (
                    <AdminTextTableComponent handler={this.handleChildInfo} catId={id} />
                )
            case "articleDetail":
                return (
                    <AdminArticleDetailComponent handler={this.handleChildInfo} artId={id} />
                )
            case "createCat":
                return (
                    <AdminCategoryCreateComponent handler={this.handleChildInfo} />
                )
            case "catEdit":
                return (
                    <AdminCategoryEditComponent handler={this.handleChildInfo} catId={id} />
                )
            case "createArt":
                return (
                    <AdminArticleCreateComponent handler={this.handleChildInfo} listCat={this.state.listCat} />
                )
            case "articleEdit":
                return (
                    <AdminArticleEditComponent handler={this.handleChildInfo} artId={id} listCat={this.state.listCat} />
                )
            default:
                return (
                    <></>
                )
        }
    }

    handleChildInfo(info: string, top: boolean) {
        if (top) {
            this.props.parentHandler(info)
        } else {
            this.setState({ content: info });
        }
    }

    async deleteCat(id: number) {
        let path = "category/delete/" + id;
        const response = await ApiService.call(path, ApiService.DELETE);
        if (response.status === 200) {
            this.setState({ loading: true, listCat: [] });
            this.loadCategories();
        }
    }

    render() {
        const items: any = [];
        if (!this.state.loading) {
            this.state.listCat.forEach((cat: any) => {
                items.push(
                    <li className="nav-item " key={cat.Id}>
                        <a className="submenuCat nav-link" href="/User"
                            onClick={(e) => {
                                e.preventDefault();
                                this.setState({ content: "home:" + cat.Id })
                            }}
                            title={cat.Description}>
                            {cat.Name}
                        </a>
                        <ul className="nav flex-column pl-3">
                            <li className="nav-item">
                                <a className="submenuLink nav-link" href="/Admin"
                                    onClick={(e) => {
                                        e.preventDefault();
                                        this.setState({ content: "catEdit:" + cat.Id });
                                    }}>Upravit</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link submenuLink" href="/Admin"
                                    onClick={(e) => {
                                        e.preventDefault();
                                        this.deleteCat(cat.Id);
                                    }}>Odstranit</a>
                            </li>
                        </ul>
                    </li>
                )
            });
        }
        return (
            <>
                <div className="nav-left-sidebar sidebar-dark">
                    <nav className="navbar navbar-expand-md navbar-light w-100">
                        <ul className="navbar-nav flex-column">
                            <li className="nav-divider">
                                <h5>Výukové texty</h5>
                            </li>
                            <li className="nav-item mb-1">

                                <ul className="nav flex-column">
                                    <a className="btn btn-primary" href="/Admin"
                                        onClick={(e) => {
                                            e.preventDefault();
                                            this.setState({ content: "createCat" });
                                        }}>Vytvořit kategorii</a>
                                </ul>
                            </li>
                            <li className="nav-item">

                                <ul className="nav flex-column">
                                    <a className="btn btn-primary" href="/Admin"
                                        onClick={(e) => {
                                            e.preventDefault();
                                            this.setState({ content: "createArt" })
                                        }}>Vytvořit článek</a>
                                </ul>
                            </li>
                            <li className="nav-item">

                                <ul className="nav flex-column">
                                    {items}
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>
                <div className="dashboard-wrapper">
                    {this.renderBody(this.state.content)}
                </div>
            </>
        );
    }
}

export default AdminTextComponent;
import React from "react";
import { ApiService } from "../../../services/ApiService";

interface Prop {
    handler: any;
    catId: number;
}

interface State {
    listText: Array<any>,
    loading: boolean
}

class AdminTextTableComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { loading: true, listText: [] };
    }

    componentDidMount() {
        this.loadArticles(this.props.catId);
    }

    componentWillReceiveProps(nextProps: any) {
        this.loadArticles(nextProps.catId);
    }

    async loadArticles(catId: number) {
        let path = "article/getAll";
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let texts: Array<any> = await response.json();
            if (catId > 0) {
                texts = texts.filter(text => text.Category.Id === catId);
            }
            this.setState({ listText: texts, loading: false });
        } if (response.status === 401) {
            this.setState({ listText: [], loading: false });
        }
    }

    async deleteArticle(id: any) {
        let path = "article/delete/" + id;
        const response = await ApiService.call(path, ApiService.DELETE);
        if (response.status === 200) {
            this.setState({ loading: true, listText: [] });
            this.loadArticles(this.props.catId);
        }
    }

    renderItems() {
        const items: any = [];
        if (!this.state.loading) {
            this.state.listText.forEach((tx: any) => {
                let testAction = "Vytvořit";
                if (tx.TestId !== 0) {
                    testAction = "Upravit";
                }
                items.push(
                    <tr key={tx.Id}>
                        <td><a href="/Admin" onClick={(e) => {
                            e.preventDefault();
                            this.props.handler("articleDetail:" + tx.Id);
                        }}>{tx.Name}</a></td>
                        <td>{tx.Author}</td>
                        <td><a href="/Admin" onClick={e => {
                            e.preventDefault();
                            this.props.handler("articleEdit:" + tx.Id)
                        }}>Upravit</a></td>
                        <td><a href="/Admin" onClick={e => {
                            e.preventDefault();
                            this.deleteArticle(tx.Id)
                        }}>Odstranit</a></td>
                        <td>
                            <a href="/Admin" onClick={(e) => {
                                e.preventDefault();
                                this.props.handler("test:" + tx.Id, true)
                            }}>{testAction}</a>
                        </td>
                    </tr>
                )
            });
        }
        return (<>
            <thead className="thead-dark">
                <tr>
                    <th scope="col">Článek</th>
                    <th scope="col">Autor článku</th>
                    <th scope="col">Úprava</th>
                    <th scope="col"></th>
                    <th scope="col">Testy</th>
                </tr>
            </thead>
            <tbody>
                {items}
            </tbody>
        </>

        )

    }

    render() {
        return (
            <div className="container-fluid dashboard-content">
                <table className="table">
                    {this.renderItems()}
                </table>
            </div>
        );
    }
}

export default AdminTextTableComponent;
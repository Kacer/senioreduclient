import React from "react";
import { ApiService } from "../../../services/ApiService";
import { isNull } from "util";

interface Prop {
    handler: any;
    artId: number;
}

interface State {
    article: any,
    loading: boolean
}

class AdminArticleDetailComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { loading: true, article: null };
    }

    componentDidMount() {
        this.loadArticle(this.props.artId);
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({ loading: true, article: null });
        this.loadArticle(nextProps.artId);
    }

    replaceAll(str: any, find: any, replace: any) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    async loadArticle(id: number) {
        let path = "article/getById/" + id;
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let art = await response.json();
            art.Description = this.replaceAll(art.Description, "src=\"", "src=\"" + ApiService.SERVER_PREFIX);
            this.setState({ article: art, loading: false });
        } if (response.status === 400 || response.status === 404) {
            this.setState({ article: null, loading: false });
        }
    }

    render() {
        if (!this.state.loading && !isNull(this.state.article)) {
            const createMarkup = (htmlString: any) => ({ __html: htmlString });
            return (
                <div className="container-fluid dashboard-content">
                    <h2>Detail článku {this.state.article.Name}</h2>
                    <p>
                        Autor: {this.state.article.Author}
                    </p>

                    <div dangerouslySetInnerHTML={createMarkup(this.state.article.Description)} />
                    <a className="btn btn-outline-primary mb-3" href="/User" onClick={(e) => {
                        e.preventDefault();
                        this.props.handler("home");
                    }}>Zpět na všechny články</a>
                </div>
            );
        }
        return (
            <></>
        )
    }

}

export default AdminArticleDetailComponent;
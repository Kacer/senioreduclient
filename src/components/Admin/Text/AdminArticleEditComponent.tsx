import React from "react";
import { FormService } from "../../../services/FormService";
import { ApiService } from "../../../services/ApiService";
import { isNull } from "util";

interface Prop {
    handler: any;
    listCat: Array<any>;
    artId: number;
}

interface State {
    article: any,
    name: string,
    description: string,
    categoryId: number,
    author: string,
    loading: boolean
    errors: {
        name: string,
        description: string,
        author: string,
        categoryId: string
    };
}

class AdminArticleEditComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            article: null,
            name: '',
            description: '',
            categoryId: 0,
            author: '',
            loading: true,
            errors: {
                name: '',
                description: '',
                author: '',
                categoryId: ''
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.loadArticle(this.props.artId);
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({ loading: true, article: null });
        this.loadArticle(nextProps.catId);
    }

    async loadArticle(id: number) {
        let path = "article/getById/" + id;
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let art = await response.json();
            this.setState({
                article: art,
                loading: false,
                name: art.Name,
                description: art.Description,
                categoryId: art.Category.Id,
                author: art.Author
            });
        } if (response.status === 400 || response.status === 404) {
            this.setState({ article: null, loading: false });
        }
    }

    handleSubmit(event: any) {
        if (this.validateForm(this.state.errors, this.state)) {
            this.sendData();
        }
        event.preventDefault();
    }

    handleChange(event: any) {
        const newState = FormService.getStateFromEvent(event);
        this.setState(newState);
        this.validateState(event.target.name, event.target.value);
    }

    validateForm(errors: any, states: any) {
        let valid = true;
        Object.keys(errors).forEach(
            (key: string) => {
                this.validateState(key, states[key]);
            }
        );
        Object.values(errors).forEach(
            (val: any) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    validateState(name: any, value: any) {
        let errorsArray = this.state.errors;
        switch (name) {
            case "name":
                errorsArray.name = value.length < 1 ? 'Název článku je vyžadován.' : '';
                break;
            case "description":
                errorsArray.description = value.length < 1 ? 'Text článku je vyžadován.' : '';
                break;
            case "author":
                errorsArray.author = value.length < 1 ? 'Autor musí být zadán.' : '';
                break;
            case "categoryId":
                errorsArray.categoryId = !value || value === 0 ? 'Kategorie musí být vybrána.' : '';
                break;
            default:
                break;
        }
        this.setState({ errors: errorsArray });
    }

    async sendData(): Promise<boolean> {
        let catFromList = null;
        this.props.listCat.forEach(cat => {
            if (cat.Id == this.state.categoryId) {
                catFromList = cat;
            }
        })

        let jsonData = {
            id: this.state.article.Id,
            name: this.state.name,
            description: this.state.description,
            author: this.state.author,
            category: catFromList
        };


        const response = await ApiService.callWithBody("article/update", ApiService.PUT, jsonData);
        if (response.status === 400) {
            console.log("Something is wrong")
        } else if (response.status === 200) {
            this.props.handler("home");
        }
        return response.ok;
    }

    render() {
        const options: any = [];
        this.props.listCat.forEach((cat: any) => {
            options.push(
                <option value={cat.Id} key={cat.Id}>{cat.Name}</option>
            )
        });
        if (!this.state.loading && !isNull(this.state.article)) {
            return (
                <div className="container-fluid dashboard-content">
                    <h2>Upravit článek - {this.state.article.Name}</h2>
                    <form onSubmit={this.handleSubmit}>
                        <div className="container">
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label" id="inputSuccess1">Název článku</label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="name" type="text" value={this.state.name} onChange={this.handleChange} />
                                    <span className="field-validation-valid">{this.state.errors.name}</span>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label" id="inputSuccess1">Autor</label>
                                <div className="col-sm-10">
                                    <input className="form-control" name="author" type="text" value={this.state.author} onChange={this.handleChange} />
                                    <span className="field-validation-valid">{this.state.errors.author}</span>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label" >Kategorie</label>
                                <div className="col-sm-10">
                                    <select className="form-control" name="categoryId"
                                        value={this.state.categoryId} onChange={this.handleChange}>
                                        <option value={0}></option>
                                        {options}
                                    </select>
                                    <span className="field-validation-valid">{this.state.errors.categoryId}</span>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Text článku</label>
                                <div className="col-sm-10">
                                    <textarea className="form-control" cols={20} name="description" rows={2}
                                        value={this.state.description} onChange={this.handleChange}></textarea>
                                    <span className="field-validation-valid">{this.state.errors.description}</span>
                                </div>
                            </div>

                            <div className="form-group">
                                <div className="col-sm-2 col-sm-10">
                                    <button type="submit" className="btn btn-primary">Upravit článek</button>
                                    <a href="/Admin" className="btn btn-primary pull-right" onClick={e => {
                                        e.preventDefault();
                                        this.props.handler("home");
                                    }}>Zpět</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            );
        } else {
            return <></>;
        }
    }
}

export default AdminArticleEditComponent;
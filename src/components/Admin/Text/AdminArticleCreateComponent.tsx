import React from "react";
import { FormService } from "../../../services/FormService";
import { ApiService } from "../../../services/ApiService";

interface Prop {
    handler: any;
    listCat: Array<any>;
}

interface State {
    name: string,
    description: string,
    categoryId: number,
    author: string
    errors: {
        name: string,
        description: string,
        author: string,
        categoryId: string
    };
}

class AdminArticleCreateComponent extends React.Component<Prop, State> {
    constructor(props: any) {
        super(props);
        this.state = {
            name: '',
            description: '',
            categoryId: 0,
            author: '',
            errors: {
                name: '',
                description: '',
                author: '',
                categoryId: ''
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event: any) {
        if (this.validateForm(this.state.errors, this.state)) {
            this.sendData();
        }
        event.preventDefault();
    }

    handleChange(event: any) {
        const newState = FormService.getStateFromEvent(event);
        this.setState(newState);
        this.validateState(event.target.name, event.target.value);
    }

    validateForm(errors: any, states: any) {
        let valid = true;
        Object.keys(errors).forEach(
            (key: string) => {
                this.validateState(key, states[key]);
            }
        );
        Object.values(errors).forEach(
            (val: any) => val.length > 0 && (valid = false)
        );
        return valid;
    }

    validateState(name: any, value: any) {
        let errorsArray = this.state.errors;
        switch (name) {
            case "name":
                errorsArray.name = value.length < 1 ? 'Název článku je vyžadován.' : '';
                break;
            case "description":
                errorsArray.description = value.length < 1 ? 'Text článku je vyžadován.' : '';
                break;
            case "author":
                errorsArray.author = value.length < 1 ? 'Autor musí být zadán.' : '';
                break;
            case "categoryId":
                errorsArray.categoryId = !value || value === 0 ? 'Kategorie musí být vybrána.' : '';
                break;
            default:
                break;
        }
        this.setState({ errors: errorsArray });
    }

    async sendData(): Promise<boolean> {
        let catFromList = null;
        this.props.listCat.forEach(cat => {
            if (cat.Id == this.state.categoryId) {
                catFromList = cat;
            }
        })

        let jsonData = {
            name: this.state.name,
            description: this.state.description,
            author: this.state.author,
            category: catFromList
        };


        const response = await ApiService.callWithBody("article/create", ApiService.POST, jsonData);
        if (response.status === 400) {
            console.log("Something is wrong")
        } else if (response.status === 200) {
            this.props.handler("home");
        }
        return response.ok;
    }

    render() {
        const options: any = [];
        this.props.listCat.forEach((cat: any) => {
            options.push(
                <option value={cat.Id} key={cat.Id}>{cat.Name}</option>
            )
        });

        return (
            <div className="container-fluid dashboard-content">
                <h2>Vytvoření nového článku</h2>
                <form onSubmit={this.handleSubmit}>
                    <div className="container">
                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label" id="inputSuccess1">Název článku</label>
                            <div className="col-sm-10">
                                <input className="form-control" name="name" type="text" value={this.state.name} onChange={this.handleChange} />
                                <span className="field-validation-valid">{this.state.errors.name}</span>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label" id="inputSuccess1">Autor</label>
                            <div className="col-sm-10">
                                <input className="form-control" name="author" type="text" value={this.state.author} onChange={this.handleChange} />
                                <span className="field-validation-valid">{this.state.errors.author}</span>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label" >Kategorie</label>
                            <div className="col-sm-10">
                                <select className="form-control" name="categoryId"
                                    value={this.state.categoryId} onChange={this.handleChange}>
                                    <option value={0}></option>
                                    {options}
                                </select>
                                <span className="field-validation-valid">{this.state.errors.categoryId}</span>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Text článku</label>
                            <div className="col-sm-10">
                                <textarea className="form-control" cols={20} name="description" rows={2}
                                    value={this.state.description} onChange={this.handleChange}></textarea>
                                <span className="field-validation-valid">{this.state.errors.description}</span>
                            </div>
                        </div>

                        <div className="form-group">
                            <div className="col-sm-2 col-sm-10">
                                <button type="submit" className="btn btn-primary">Vytvořit článek</button>
                                <a href="/Admin" className="btn btn-primary pull-right" onClick={e => {
                                    e.preventDefault();
                                    this.props.handler("home");
                                }}>Zpět</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default AdminArticleCreateComponent;
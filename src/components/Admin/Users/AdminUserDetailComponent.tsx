import React from "react";
import { ApiService } from "../../../services/ApiService";
import { isNull } from "util";

interface Prop {
    handler: any;
    id: number;
}

interface State {
    user: any,
    loading: boolean
}

class AdminUserDetailComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { loading: true, user: null };
    }

    componentDidMount() {
        this.loadUser(this.props.id);
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({ loading: true, user: null });
        this.loadUser(nextProps.type);
    }

    async loadUser(id: number) {
        let path = "user/getById/" + id;
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let u = await response.json();
            this.setState({ user: u, loading: false });
        } if (response.status === 400 || response.status === 404) {
            this.setState({ user: null, loading: false });
        }
    }

    render() {
        if (!this.state.loading && !isNull(this.state.user)) {
            let birth=<></>;
            if (!isNull(this.state.user.Birth)) {
                birth =  <p>
                Datum narození: {this.state.user.Birth}
                </p>;
            }          
            return (
                <>
                    <div className="container-fluid dashboard-content">
                        <h2>Detail uživatele</h2>
                        <h3>Uživatel {this.state.user.Login}</h3>
                        <p>
                            Jméno: {this.state.user.Name + " " + this.state.user.Surname}
                        </p>
                        {birth}
                        <p>
                            Datum registrace:  {this.state.user.RegistrationDay}
                        </p>
                        <p>
                            Datum posledního přihlášení: {this.state.user.LastActivityDay}
                        </p>
                    </div>
                </>
            );
        } 
        return(
            <></>
        )
    }
}

export default AdminUserDetailComponent;
import React from "react";
import { ApiService } from "../../../services/ApiService";

interface Prop {
    handler: any;
    type: string;
}

interface State {
    listUsers: Array<any>,
    loading: boolean
}

class AdminUsersTableComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { loading: true, listUsers: [] };
    }

    componentDidMount() {
        this.loadUsers(this.props.type);
    }

    componentWillReceiveProps(nextProps: any) {
        this.setState({ loading: true, listUsers: [] });
        this.loadUsers(nextProps.type);
    }

    async loadUsers(type: string) {
        let path = "user/getUsers";
        if (type == "admin") {
            path = "user/getAdmins";
        }
        const response = await ApiService.call(path, ApiService.GET);
        if (response.status === 200) {
            let users = await response.json();
            this.setState({ listUsers: users, loading: false });
        } if (response.status === 401) {
            this.setState({ listUsers: [], loading: false });
        }
    }

    async updateUser(user: any) {
        let path = "user/update";
        const response = await ApiService.callWithBody(path, ApiService.PUT, user);
        if (response.status === 200) {
            this.setState({ loading: true, listUsers: [] });
            this.loadUsers(this.props.type);
        }
    }

    renderItems() {
        const items: any = [];
        if (!this.state.loading) {
            this.state.listUsers.forEach((u: any) => {
                items.push(
                    <tr key={u.Id}>
                        <td><a href="/Admin"
                            onClick={e => { e.preventDefault(); this.props.handler("detail:" + u.Id) }}>{u.Login}</a></td>
                        <td><a href="/Admin"
                            onClick={e => { e.preventDefault(); this.changeRole(u) }}>
                            {this.props.type === "user" ? "Jmenovat administrátorem" : "Zrušit oprávnění"}
                        </a></td>
                    </tr>
                )
            });
        }
        return (<>
            <thead className="thead-dark">
                <tr>
                    <th scope="col">Uživatel</th>
                    <th scope="col">Akce</th>
                </tr>
            </thead>
            <tbody>
                {items}
            </tbody>
        </>

        )

    }
    changeRole(user: any) {
        if (user.Role.Id === 1) {
            user.Role.Id = 2;
        } else {
            user.Role.Id = 1;
        }
        this.updateUser(user)
    }

    render() {
        return (
            <>
                <div className="container-fluid dashboard-content">
                    <div className="row">
                        <div className="col-9">
                            <table className="table">
                                {this.renderItems()}
                            </table>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default AdminUsersTableComponent;
import React from "react";
import AdminUsersTableComponent from "./AdminUsersTableComponent";
import AdminUserDetailComponent from "./AdminUserDetailComponent";

interface Prop {
    parentHandler: any;
}

interface State {
    content: string;
}

class AdminUsersComponent extends React.Component<Prop, State> {
    constructor(props: Prop) {
        super(props);
        this.state = { content: "user" };
        this.handleChildInfo = this.handleChildInfo.bind(this);
    }

    componentWillReceiveProps(){
        if (this.state.content.includes("detail")){
            this.setState({content: "user"})
        }
    }

    renderBody(page: string) {
        let id = -1;
        if(page.includes(":")){
            id = parseInt(page.substr(page.indexOf(":")+1));
            page =  page.substr(0, page.indexOf(':')); 
        }
        switch (page) {
            case "user":
                return (
                    <AdminUsersTableComponent type="user" handler={this.handleChildInfo}/>
                )
            case "admin":
                return (
                    <AdminUsersTableComponent type="admin" handler={this.handleChildInfo}/>
                )
            case "detail":
                return(
                    <AdminUserDetailComponent id={id} handler={this.handleChildInfo}/>
                )
            default:
                return (
                    <></>
                )
        }
    }

    handleChildInfo(info: string, top: boolean) {
        if (top) {
            this.props.parentHandler(info)
        } else {
            this.setState({ content: info });
        }
    }

    render() {
        return (
            <>
                <div className="nav-left-sidebar sidebar-dark">
                    <nav className="navbar navbar-expand-md navbar-light w-100">
                        <ul className="navbar-nav flex-column">
                            <li className="nav-divider">
                                <h5>Přehled uživatelů</h5>
                            </li>
                            <li className="nav-item">
                                <ul className="nav flex-column">
                                    <a className="nav-link" href="/Admin"
                                        onClick={e => { e.preventDefault(); this.setState({content: "admin"})}}>Administrátor</a>
                                    <a className="nav-link" href="/Admin"
                                    onClick={e => { e.preventDefault(); this.setState({content: "user"})}}>Uživatel</a>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div className="dashboard-wrapper">
                    {this.renderBody(this.state.content)}
                </div>
            </>
        );
    }
}

export default AdminUsersComponent;